#include <stdio.h>
#include <stdbool.h>

typedef struct machine_s machine_t;
struct machine_s {
	unsigned long next_eip;
};

typedef struct decode_s decode_t;
struct decode_s {
	unsigned long decode_eip;
	const bool (*emitfn)(machine_t *M, decode_t *ds);
};

typedef const bool (*EMITFN)(machine_t *M, decode_t *ds);

#ifdef INLINE_EMITTERS
#define INLINE static inline
#else
#define INLINE static
#endif

#define DS_QUEUE_MAX (5)
decode_t ds_queue[DS_QUEUE_MAX];
size_t ds_head, ds_tail;
INLINE bool enqueue_ds(decode_t* ds)
{
	if ((ds_tail+1) % DS_QUEUE_MAX == ds_head) return false;
	ds_queue[ds_tail] = *ds;
	ds_tail = (ds_tail+1) % DS_QUEUE_MAX;
	return true;
}
INLINE decode_t* dequeue_ds()
{
	if (ds_tail == ds_head) return NULL;
	decode_t* ds = &ds_queue[ds_head];
	ds_head = (ds_head+1) % DS_QUEUE_MAX;
	return ds;
}
INLINE void clear_ds()
{	
	ds_head = ds_tail = 0;
}
INLINE int count_ds()
{
	int count = ds_tail - ds_head;
	if (count < 0) count += DS_QUEUE_MAX;
	return count;
}
INLINE bool is_full_ds()
{
	return ((ds_tail+1) % DS_QUEUE_MAX == ds_head);
}
INLINE bool is_empty_ds()
{
	return (ds_head == ds_tail);
}

INLINE void print_ds()
{
	int i;
	if (ds_head == ds_tail) printf("decode queue is empty!\n");
	else printf("decode queue size: %d/%d\n", count_ds(), DS_QUEUE_MAX-1);
	for (i = ds_head; i != ds_tail; i = (i + 1) % DS_QUEUE_MAX) {
		printf("[%d] emitfn: %x\n", i, (int)ds_queue[i].emitfn);
	}
	printf("\n");
}

int main(int argc, char* argv[])
{
	decode_t ds0, ds1, ds2, ds3, ds4, ds5, ds6, ds7;
	ds0.emitfn = (EMITFN)0;
	ds1.emitfn = (EMITFN)1;
	ds2.emitfn = (EMITFN)2;
	ds3.emitfn = (EMITFN)3;
	ds4.emitfn = (EMITFN)4;
	ds5.emitfn = (EMITFN)5;
	ds6.emitfn = (EMITFN)6;
	ds7.emitfn = (EMITFN)7;

	if (!enqueue_ds(&ds0)) { 
		fprintf(stderr, "ds0 not enqueued\n"); 
		return 1; 
	}
	print_ds();

	if (!enqueue_ds(&ds1)) {
		fprintf(stderr, "ds1 not enqueued\n");
		return 1;
	}
	print_ds();
	
	if (!enqueue_ds(&ds2)) {
		fprintf(stderr, "ds2 not enqueued\n");
		return 1;
	}
	print_ds();

	decode_t* tmp = dequeue_ds();
	if (!tmp) { 
		fprintf(stderr, "ds0 not dequeued\n"); 
		return 1; 
	}
	printf("ds0.emitfn = %x\n", (int)tmp->emitfn);
	print_ds();

	tmp = dequeue_ds();
	if (!tmp) { 
		fprintf(stderr, "ds1 not dequeued\n"); 
		return 1; 
	}
	printf("ds1.emitfn = %x\n", (int)tmp->emitfn);
	print_ds();

	if (!enqueue_ds(&ds3)) {
		fprintf(stderr, "ds3 not enqueued\n");
		return 1;
	}
	print_ds();

	if (!enqueue_ds(&ds4)) {
		fprintf(stderr, "ds4 not enqueued\n");
		return 1;
	}
	print_ds();

	if (!enqueue_ds(&ds5)) {
		fprintf(stderr, "ds5 not enqueued\n");
		return 1;
	}
	print_ds();

	if (enqueue_ds(&ds6)) {
		fprintf(stderr, "ds6 supposed to be not enqueued\n");
		return 1;
	}
	printf("ds6 not enqueued becuase the queue is full!\n");
	print_ds();

	if (!is_full_ds()) {
		fprintf(stderr, "queue supposed to be full\n");
		return 1;
	}
	printf("decode queue is full!\n");
	printf("\n");

	clear_ds();

	if (!is_empty_ds()) {
		fprintf(stderr, "queue supposed to be empty\n");
		return 1;
	}
	print_ds();

	if (!enqueue_ds(&ds7)) {
		fprintf(stderr, "ds7 not enqueued\n");
		return 1;
	}
	print_ds();

	return 0;
}
