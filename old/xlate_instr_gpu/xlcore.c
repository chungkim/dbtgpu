/*
 * Copyright (c) 2005, Johns Hopkins University and The EROS Group, LLC.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *
 *  * Neither the name of the Johns Hopkins University, nor the name
 *    of The EROS Group, LLC, nor the names of their contributors may
 *    be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdbool.h>
#include <stdio.h>
#include <signal.h>
//#include <asm/sigcontext.h>
#include <bits/sigcontext.h>
#include <sys/mman.h>
#include "switches.h"
#include "debug.h"
#include "util.h"
#include "machine.h"
#include "decode.h"
#include "emit.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/user.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>


#ifdef INLINE_EMITTERS
#define INLINE static inline
#else
#define INLINE static
#endif


/* DBT-GPU */

#define DS_QUEUE_MAX (4096)
decode_t ds_queue[DS_QUEUE_MAX];
size_t ds_head, ds_tail;
INLINE bool enqueue_ds(decode_t* ds)
{
	if ((ds_tail+1) % DS_QUEUE_MAX == ds_head) return false;
        ds_queue[ds_tail] = *ds;
        ds_tail = (ds_tail+1) % DS_QUEUE_MAX;
        return true;
}
INLINE decode_t* dequeue_ds()
{
        if (ds_tail == ds_head) return NULL;
        decode_t* ds = &ds_queue[ds_head];
        ds_head = (ds_head+1) % DS_QUEUE_MAX;
        return ds;
}
INLINE void clear_ds()
{
	ds_head = ds_tail = 0;
}
INLINE bool is_full_ds()
{
	return ((ds_tail+1) % DS_QUEUE_MAX == ds_head);
}
INLINE bool is_empty_ds()
{
	return (ds_head == ds_tail);
}

INLINE void print_ds()
{
	int i;
	if (ds_head == ds_tail) {
	       printf("decode queue is empty!\n");
	}
	for (i = ds_head; i != ds_tail; i = (i + 1) % DS_QUEUE_MAX) {
		printf("[%d] emitfn: %x\n", i, (int)ds_queue[i].emitfn);
	}
	// printf("\n");
}

unsigned char* bbOut_bak;
INLINE void set_bbOut(unsigned char* bbOut)
{
	bbOut_bak = bbOut;
}
INLINE unsigned char* get_bbOut()
{
	return bbOut_bak;
}

unsigned long next_eip_bak;
INLINE void set_next_eip(unsigned long next_eip)
{
	next_eip_bak = next_eip;
}
INLINE unsigned long get_next_eip()
{
	return next_eip_bak;
}

bb_entry* curr_bb_entry_bak;
INLINE void set_curr_bb_entry(bb_entry* curr_bb_entry)
{
	curr_bb_entry_bak = curr_bb_entry;
}
INLINE bb_entry* get_curr_bb_entry()
{
	return curr_bb_entry_bak;
}

bool emit_switch;



/* For the sake of DEBUG_RET_CACHE */
FILE *DBG;
/* end DEBUG_RET_CACHE */

#define THIRTY_TWO_BIT_INSTR(d)  ( (d->opstate & OPSTATE_DATA32) ? 0x0001u : 0x0000u )
bb_entry * xlate_bb(machine_t *M);

/* The only purpose of these macros is to document the role that
   different functions are taking, but they are useful to have. */
#define TARGET_CALLED
#define ENTRY_POINT

#ifdef INLINE_EMITTERS
#include "emit-inline.c"

/* emit_normal() repeated here for inlining purposes */
/* WARNING: Changes should be done in emit.c as well */
inline static bool 
inline_emit_normal(machine_t *M, decode_t *d)
{
  unsigned i;
  unsigned count = M->next_eip - d->decode_eip;
#ifdef PROFILE
  M->s_normal_cnt++;
  bb_emit_inc(M, MFLD(M, normal_cnt));
#endif

  DEBUG(emits)
    printf("%lu: Normal %lx %s!\n", M->nTrInstr, d->decode_eip, ((OpCode *)d->pEntry)->disasm);

#ifdef STATIC_PASS
  memcpy(M->bbOut, (unsigned char *)d->mem_decode_eip, count);
#else
  memcpy(M->bbOut, (unsigned char *)d->decode_eip, count);
#endif
  M->bbOut += count;

  return false;
}
#endif /* INLINE_EMITTERS */

#ifdef USE_SIEVE
#ifdef SEPARATE_SIEVES 
#include "chtable.c"
#endif
#endif

/* WARNING: Changes should be made in emit.c as well */
/* Repeated here for reasons of inlining */
inline static bb_entry *
make_bb_entry(machine_t *M, unsigned long src, unsigned long dest, unsigned long p) 						
{						
  bb_entry *new_entry;
  bb_entry **lookup_table_entry;	

  if(M->no_of_bbs >= MAX_BBS)	
    panic("MAX_BBS exceededn");

  new_entry = &((M->bb_entry_nodes)[(M->no_of_bbs)++]);
  new_entry->src_bb_eip = src;
  new_entry->trans_bb_eip = dest; 
  new_entry->sieve_header = NULL;
  new_entry->proc_entry = p;

#ifdef DIFF_HASH
  lookup_table_entry = &M->lookup_table[((src) - M->guest_start_eip) & (LOOKUP_TABLE_SIZE - 1)];
#else
  lookup_table_entry = &M->lookup_table[(src) & (LOOKUP_TABLE_SIZE - 1)];	
#endif

  new_entry->next = *lookup_table_entry;
  *lookup_table_entry = new_entry;

#ifdef PROFILE_BB_STATS  
  new_entry->trace_next = NULL;
  new_entry->nInstr = 0;
  new_entry->flags = 0;
#endif

#ifdef PROFILE_RET_MISS  
  if(p == (unsigned long)COLD_PROC_ENTRY)
    M->cold_cnt++;
#endif

  return new_entry;
}

ENTRY_POINT bb_entry * 
lookup_bb_eip(machine_t *M, unsigned long src_eip)
{
#ifdef USE_DIFF_HASH									
  bb_entry *curr =  M->lookup_table[((src_eip-M->guest_start_eip) & (LOOKUP_TABLE_SIZE - 1))];
#else
  bb_entry *curr =  M->lookup_table[(src_eip & (LOOKUP_TABLE_SIZE - 1))];
#endif

  while ((curr != NULL) && (curr->src_bb_eip != src_eip))
    curr = curr->next;
  DEBUG(lookup) {
    if(curr == NULL)
      printf("\nLooking up %lx Failed\n", src_eip);
    else 
      printf("\nLooking up %lx Success\n", src_eip);
  }
  return curr;
}

#ifdef USE_SIEVE
INLINE void 
bb_setup_hash_table(machine_t *M)
{
  int i;
  for (i=0 ; i<NBUCKETS ; i++)  {
    bb_emit_jump(M, M->slow_dispatch_bb);
    M->bbOut += 3;
  }
}
#endif /* USE_SIEVE */ 


void 
emit_sieve_header_if_bb_present_or_else_xlate_bb (machine_t *M)
{
  bb_entry *entry_node;
  bucket_entry *bucket;	/* Hash bucket onto which this basic-block 
			   will chain onto (at the head)   	*/
  unsigned char *next_instr; /* Sequentially next instr of the above - 
				used just to compute relative jump destination	*/
  unsigned long node; /* A node of the hash chain */

  M->comming_from_call_indirect = false;

  // printf("Calling xlate_bb at %lx\n", M->fixregs.eip);
  entry_node = xlate_bb(M);

#ifdef USE_SIEVE
/*   bucket =  */
/*     (bucket_entry *)(M->hash_table + (((unsigned long)entry_node->src_bb_eip) & SIEVE_HASH_MASK)); */

  bucket = (bucket_entry *) SIEVE_HASH_BUCKET(M->hash_table, ((unsigned long)entry_node->src_bb_eip));
  //  printf("Hash bucket start at %lx, this = %lx\n", M->hash_table, bucket);

  /*** Sensitive to the size of Jump Instruction in Bucket ***/
  next_instr = ((unsigned char *)bucket) + 5  ;

  node = (unsigned long) (next_instr + bucket->rel);
  bucket->rel = M->bbOut - next_instr;

/*   printf("Bucket #%ld seip = %lx teip = %lx at %lx\n",  */
/* 	 ((unsigned char*)bucket - (M->hash_table))/sizeof(bucket_entry), */
/* 	 entry_node->src_bb_eip, */
/* 	 entry_node->trans_bb_eip, */
/* 	 M->bbOut); */
/*   fflush(stdout); */
  
#ifdef SIEVE_WITHOUT_PPF
  /* mov 0x4(%esp),%ecx */
  bb_emit_byte(M, 0x8bu); // 8b /r
  bb_emit_byte(M, 0x4cu); // 01 001 100
  bb_emit_byte(M, 0x24u); // 00 100 100
  bb_emit_byte(M, 0x4u);

  /* lea -entry->src_bb_eip(%ecx),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x89u); // 10 001 001 
  bb_emit_w32(M, (-((long)(entry_node->src_bb_eip))));

  /* jecxz equal */
  bb_emit_byte(M, 0xe3u);
  bb_emit_byte(M, 0x05u);

  /* jmp $next_bucket */
  bb_emit_jump(M, (unsigned char *)node);

  /* equal: pop %ecx */
  bb_emit_byte(M, 0x59u);

  /* leal 4(%esp) %esp */
  bb_emit_byte(M, 0x8du); // 8d /r
  bb_emit_byte(M, 0x64u); // 01 100 100
  bb_emit_byte(M, 0x24u); // 00 100 100
  bb_emit_byte(M, 0x4u);

  /* jmp $translated_block */
  bb_emit_jump (M, (unsigned char *)entry_node->trans_bb_eip);
#else

   /* cmpl $entry->src_bb_eip, (%esp) */
  bb_emit_byte(M, 0x81u); // 81 /7
  bb_emit_byte(M, 0x7cu); // 01 111 100
  bb_emit_byte(M, 0x24u); // 00 100 100
  bb_emit_byte(M, 0x4u);
  bb_emit_w32(M, entry_node->src_bb_eip);

  /* jne $(next_bb) */
  bb_emit_byte (M, 0x0Fu);
  bb_emit_byte (M, 0x85u);
  bb_emit_w32 (M, node - ((unsigned long)M->bbOut + 4));

  /* popf */
  bb_emit_byte (M, 0x9Du);

  /* leal 4(%esp) %esp */
  bb_emit_byte(M, 0x8du); // 8d /r
  bb_emit_byte(M, 0x64u); // 01 100 100
  bb_emit_byte(M, 0x24u); // 00 100 100
  bb_emit_byte(M, 0x4u);

  /* jmp $translated_block */
  bb_emit_jump (M, (unsigned char *)entry_node->trans_bb_eip);
#endif /* SIEVE_WITHOUT_PPF */
 
  M->jmp_target = (unsigned char *)entry_node->trans_bb_eip;
#ifdef PROFILE
  M->hash_nodes_cnt++;
#endif
#endif /* USE_SIEVE */
}

INLINE void 
bb_setup_startup_slow_dispatch_bb(machine_t *M)
{
  /* Emit the special BB that first translates the destination basic-block 
     and then transfers control into the basic block. */

  bb_emit_byte(M, 0x68u);		/* PUSH imm32:M */
  bb_emit_w32(M, (unsigned long)M);     /* Argument for emit_sieve_ ... */
  bb_emit_byte(M, 0xE8u);		/* CALL emit_sieve_header_if_bb_present_or_else_xlate_bb */
  bb_emit_w32(M, M->emit_sieve_header_if_bb_present_or_else_xlate_bb - (M->bbOut + 4));
  bb_emit_byte (M, 0x58u); 		/* POP r32: EAX */

  bb_emit_byte(M, 0x61u);		/* POPA: UserEntry who is my caller would have already done a pusha! */
  bb_emit_byte(M, 0x9du);		/* POPF: UserEntry who is my caller would have already done a pushf! */

  //bb_emit_byte(M, 0x65u); /* GS Segment Override Prefix - for accessing the M structure */
  bb_emit_byte(M, 0xFFu); /* JMP [jmp_target] */
  bb_emit_byte(M, 0x25u); /* 00 100 101 */
  bb_emit_w32(M, MFLD(M, jmp_target)); /* Jump to the newly translated basic-block */
}


#ifdef USE_SIEVE
#ifdef SIEVE_WITHOUT_PPF

INLINE void 
bb_setup_slow_dispatch_bb(machine_t *M)
{
  /* Emit the special BB that first translates the destination basic-block 
     Found and then transfers control into the basic block. */

  /*  fast_dispatch_bb who is my caller, would have done a Push %ecx */
  /* Pop %ecx */
  bb_emit_byte(M, 0x59u);

  /* pop M->fixregs.eip */
  bb_emit_byte(M, 0x8Fu); // 8F /0
  bb_emit_byte(M, 0x05u); // 00 000 101
  bb_emit_w32(M, MREG(M, eip));

  bb_emit_byte(M, 0x9cu);		/* PUSHF */
  bb_emit_byte(M, 0x60u);		/* PUSHA */

  bb_emit_byte(M, 0x68u);		/* PUSH imm32:M */
  bb_emit_w32(M, (unsigned long)M);     /* Argument for emit_sieve_ ... */
  bb_emit_byte(M, 0xE8u);		/* CALL emit_sieve_header_if_bb_present_or_else_xlate_bb */
  bb_emit_w32(M, M->emit_sieve_header_if_bb_present_or_else_xlate_bb - (M->bbOut + 4));
  bb_emit_byte (M, 0x58u); 		/* POP r32: EAX */

  bb_emit_byte(M, 0x61u);		/* POPA */
  bb_emit_byte(M, 0x9du);		/* POPF */

  //bb_emit_byte(M, 0x65u); /* GS Segment Override Prefix - for accessing the M structure */
  bb_emit_byte(M, 0xFFu); /* JMP [jmp_target] */
  bb_emit_byte(M, 0x25u); /* 00 100 101 */
  bb_emit_w32(M, MFLD(M, jmp_target)); /* Jump to the newly translated basic-block */
}

#else

INLINE void 
bb_setup_slow_dispatch_bb(machine_t *M)
{
  /* Emit the special BB that first translates the destination basic-block 
     Found and then transfers control into the basic block. */

  /* pop M->eflags */
  bb_emit_byte(M, 0x8Fu); // 8F /0
  bb_emit_byte(M, 0x05u); // 00 000 101
  bb_emit_w32(M, MFLD(M, eflags));

  /* pop M->fixregs.eip */
  bb_emit_byte(M, 0x8Fu); // 8F /0
  bb_emit_byte(M, 0x05u); // 00 000 101
  bb_emit_w32(M, MREG(M, eip));

  // Better than popf and then pushf and use of intermediate register?
  /* push M->eflags */
  bb_emit_byte(M, 0xFFu); // FF /6
  bb_emit_byte(M, 0x35u); // 00 110 101
  bb_emit_w32(M, MFLD(M, eflags));

  bb_emit_byte(M, 0x60u);		/* PUSHA */

  bb_emit_byte(M, 0x68u);		/* PUSH imm32:M */
  bb_emit_w32(M, (unsigned long)M);     /* Argument for emit_sieve_ ... */
  bb_emit_byte(M, 0xE8u);		/* CALL emit_sieve_header_if_bb_present_or_else_xlate_bb */
  bb_emit_w32(M, M->emit_sieve_header_if_bb_present_or_else_xlate_bb - (M->bbOut + 4));
  bb_emit_byte (M, 0x58u); 		/* POP r32: EAX */

  bb_emit_byte(M, 0x61u);		/* POPA */

  bb_emit_byte(M, 0x9du);		/* POPF: fast_dispatch_bb who is my caller would have already done a pushf! */

  //bb_emit_byte(M, 0x65u); /* GS Segment Override Prefix - for accessing the M structure */
  bb_emit_byte(M, 0xFFu); /* JMP [jmp_target] */
  bb_emit_byte(M, 0x25u); /* 00 100 101 */
  bb_emit_w32(M, MFLD(M, jmp_target)); /* Jump to the newly translated basic-block */
}
#endif /* SIEVE_WITHOUT_PPF */
#else /* USE_SIEVE */
INLINE void 
bb_setup_slow_dispatch_bb(machine_t *M)
{
  /* Emit the special BB that first translates the destination basic-block 
     Found and then transfers control into the basic block. */

  /* pop M->fixregs.eip */
  bb_emit_byte(M, 0x8Fu); // 8F /0
  bb_emit_byte(M, 0x05u); // 00 000 101
  bb_emit_w32(M, MREG(M, eip));

  bb_emit_byte(M, 0x9cu);	        /* PUSHF */
  bb_emit_byte(M, 0x60u);		/* PUSHA */

  bb_emit_byte(M, 0x68u);		/* PUSH imm32:M */
  bb_emit_w32(M, (unsigned long)M);     /* Argument for emit_sieve_ ... */
  bb_emit_byte(M, 0xE8u);		/* CALL emit_sieve_header_if_bb_present_or_else_xlate_bb */
  bb_emit_w32(M, M->emit_sieve_header_if_bb_present_or_else_xlate_bb - (M->bbOut + 4));
  bb_emit_byte (M, 0x58u); 		/* POP r32: EAX */

  bb_emit_byte(M, 0x61u);		/* POPA */

  bb_emit_byte(M, 0x9du);	        /* Popf */

  //bb_emit_byte(M, 0x65u); /* GS Segment Override Prefix - for accessing the M structure */
  bb_emit_byte(M, 0xFFu); /* JMP [jmp_target] */
  bb_emit_byte(M, 0x25u); /* 00 100 101 */
  bb_emit_w32(M, MFLD(M, jmp_target)); /* Jump to the newly translated basic-block */
}
#endif /* USE_SIEVE */

#if 0
TARGET_CALLED static void 
prepare_for_backpatch(machine_t *M)
{
  M->fixregs.eip = *((unsigned long *)(M->backpatch_block));
  //printf("Backpatch_and_Dispatch is being called with M->eip = %08X\n", M->fixregs.eip);
  //fflush(stdout);
  M->patch_point = (unsigned char *)(*((unsigned long *)((unsigned char *)(M->backpatch_block) + 4)));
  M->comming_from_call_indirect = false;
}

TARGET_CALLED static void
backpatch(machine_t *M)
{
  *((unsigned long *)(M->patch_point)) = (unsigned long)M->jmp_target - (unsigned long)(M->patch_point + 4);
}
#endif

INLINE void
bb_setup_prepare_for_backpatch (machine_t *M)
{
  // Achieves:
  // M->fixregs.eip = *((unsigned long *)(M->backpatch_block));
  // M->patch_point = (unsigned char *)(*((unsigned long *)((unsigned char *)(M->backpatch_block) + 4)));
  // M->comming_from_call_indirect = false;
  
  // mov    $M,%edx
  bb_emit_byte(M, 0xBAu); // b8+rd
  bb_emit_w32(M, (unsigned long) M); 

  // mov    (offset(backpatch_block))(%edx),%ecx
  bb_emit_byte(M, 0x8b);
  bb_emit_byte(M, 0x8a);
  bb_emit_w32(M, offsetof(machine_t, backpatch_block));
  
  // mov    (%ecx), %eax
  bb_emit_byte(M, 0x8bu);
  bb_emit_byte(M, 0x01u);

  // mov    %eax, (offset(fixregs.eip))(%edx)
  bb_emit_byte(M, 0x89u); // 89 /r
  bb_emit_byte(M, 0x82u); // 01 000 010 
  bb_emit_w32(M, offsetof(machine_t, fixregs.eip));
  
  // mov    0x4(%ecx),%eax
  bb_emit_byte(M, 0x8b);
  bb_emit_byte(M, 0x41);
  bb_emit_byte(M, 0x04);

  // mov    %eax, (offset(patch_point))(%edx)
  bb_emit_byte(M, 0x89u); // 89 /r
  bb_emit_byte(M, 0x82u); // 10 000 010 
  bb_emit_w32(M, offsetof(machine_t, patch_point));
  
  // movb   $0x0, (offset(comming_from_call_indirect))(%edx)   
  bb_emit_byte(M, 0xC6);  // C6 /0
  bb_emit_byte(M, 0x82u); // 10 000 010 
  bb_emit_w32(M, offsetof(machine_t, comming_from_call_indirect));
  bb_emit_byte(M, 0x0u);
}

INLINE void
bb_setup_backpatch (machine_t *M)
{
  // Achieves:
  // *((unsigned long *)(M->patch_point)) = 
  //    (unsigned long)M->jmp_target - (unsigned long)(M->patch_point + 4);
  
  // mov    $M,%eax
  bb_emit_byte(M, 0xB8u); // b8+rd
  bb_emit_w32(M, (unsigned long) M); 
  
  // mov    (offfset(patchpoint))(%eax),%edx
  bb_emit_byte(M, 0x8bu);
  bb_emit_byte(M, 0x90u);
  bb_emit_w32(M, offsetof(machine_t, patch_point));
  
  // mov    (offset(jmp_target))(%eax),%eax
  bb_emit_byte(M, 0x8bu);
  bb_emit_byte(M, 0x80u);
  bb_emit_w32(M, offsetof(machine_t, jmp_target));

  // sub    %edx,%eax
  bb_emit_byte(M, 0x29u);
  bb_emit_byte(M, 0xd0u);

  // sub    $0x4,%eax
  bb_emit_byte(M, 0x83u);
  bb_emit_byte(M, 0xe8u);
  bb_emit_byte(M, 0x04u);
  
  // mov    %eax,(%edx)
  bb_emit_byte(M, 0x89u);
  bb_emit_byte(M, 0x02u);
}


INLINE void
bb_setup_backpatch_and_dispatch_bb (machine_t *M)
{
  //bb_emit_byte(M, 0x65u); /* GS Segment Override Prefix - for accessing the M structure */
  bb_emit_byte(M, 0x8Fu); /* POP M->backpatch_block */
  bb_emit_byte(M, 0x05u); /* 00 000 101 */
  bb_emit_w32(M, MFLD(M, backpatch_block));

  bb_emit_byte(M, 0x9cu);		/* PUSHF */
  bb_emit_byte(M, 0x60u);		/* PUSHA */
  
  bb_setup_prepare_for_backpatch(M);
  
  bb_emit_byte(M, 0x68u);		/* PUSH imm32:M */
  bb_emit_w32(M, (unsigned long)M);
  bb_emit_byte(M, 0xE8u);		/* CALL xlate_bb */
  bb_emit_w32(M, M->xlate_bb - (M->bbOut + 4));
  bb_emit_byte (M, 0x58u); 		/* POP r32: EAX */

  bb_setup_backpatch(M);

  bb_emit_byte(M, 0x61u);		/* POPA */
  bb_emit_byte(M, 0x9du);		/* POPF */

  //bb_emit_byte(M, 0x65u); /* GS Segment Override Prefix - for accessing the M structure */
  bb_emit_byte(M, 0xFFu); 		/* JMP [jmp_target] */
  bb_emit_byte(M, 0x25u); 		/* 00 100 101 */
  bb_emit_w32(M, MFLD(M, jmp_target));  /* Jump to the newly translated basic-block */
  
  /* This function previously used calls at runtime. as in: */
  /*   bb_emit_byte(M, 0x68u);		// PUSH imm32:M  */
  /*   bb_emit_w32(M, (unsigned long)M); */
  /*   bb_emit_byte(M, 0xE8u);		// CALL prepare_for_backpatch */
  /*   bb_emit_w32(M, ((unsigned char*)prepare_for_backpatch) - (M->bbOut + 4)); */
  /*   bb_emit_byte (M, 0x58u); 		// POP r32: EAX */
}

#ifdef USE_SIEVE
INLINE void
bb_setup_call_calls_fast_dispatch_bb(machine_t *M)
{
  /* Emit the special BB that transfers control from
     one basic-block to another within the basic block cache. */

#ifdef PROFILE_RET_MISS
  bb_emit_inc(M, MFLD(M, ret_miss_cnt));
#endif

  /* mov 0x4(%esp),%ecx */
  bb_emit_byte(M, 0x8bu); // 8b /r
  bb_emit_byte(M, 0x4cu); // 01 001 100
  bb_emit_byte(M, 0x24u); // 00 100 100
  bb_emit_byte(M, 0x4u);

#ifndef SMALL_HASH
  /* lea 0x0(,%ecx,2),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x0Cu); // 00 001 100
  bb_emit_byte(M, 0x4du); // 01 001 101
  bb_emit_w32(M, 0x0u);   // This 0 word is needed. 
                          // There is no other addressing mode.

  /* movzwl %cx,%ecx */
  bb_emit_byte(M, 0x0Fu); // 0F B7 /R
  bb_emit_byte(M, 0xB7u);
  bb_emit_byte(M, 0xC9u); // 11 001 001

  /* lea M->hash_table(,%ecx,4),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x0Cu); // 00 001 100
  bb_emit_byte(M, 0x8du); // 10 001 101
  bb_emit_w32(M, (unsigned long)M->hash_table);

#else
  /* lea 0x0(,%ecx,4),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x0Cu); // 00 001 100
  bb_emit_byte(M, 0x8du); // 10 001 101
  bb_emit_w32(M, 0x0u);   // This 0 word is needed. 
                          // There is no other addressing mode.

  /* movzwl %cx,%ecx */
  bb_emit_byte(M, 0x0Fu); // 0F B7 /R
  bb_emit_byte(M, 0xB7u);
  bb_emit_byte(M, 0xC9u); // 11 001 001

  /* lea M->hash_table(,%ecx,2),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x0Cu); // 00 001 100
  bb_emit_byte(M, 0x4du); // 01 001 101
  bb_emit_w32(M, (unsigned long)M->hash_table);
#endif


  //50:	ff e1                	jmp    *%ecx
  bb_emit_byte(M, 0xffu);
  bb_emit_byte(M, 0xe1u);
/*   /\* jmp *M->hash_table(,%ecx,4) *\/ */
/*   bb_emit_byte(M, 0xFFu); // FF /4 */
/*   bb_emit_byte(M, 0x24u); // 00 100 100 */
/*   bb_emit_byte(M, 0x8Du); // 10 001 101 */
/*   bb_emit_w32(M, (unsigned long) M->hash_table); */
}

INLINE void
bb_setup_ret_calls_fast_dispatch_bb(machine_t *M)
{
  /* Emit the special BB that transfers control from
     one basic-block to another within the basic block cache. */

#ifdef PROFILE_RET_MISS
  bb_emit_inc(M, MFLD(M, ret_ret_miss_cnt));
#endif

  /* push %ecx */
  bb_emit_byte(M, 0x51u);

  /* mov 0x4(%esp),%ecx */
  bb_emit_byte(M, 0x8bu); // 8b /r
  bb_emit_byte(M, 0x4cu); // 01 001 100
  bb_emit_byte(M, 0x24u); // 00 100 100
  bb_emit_byte(M, 0x4u);

#ifndef SMALL_HASH
  /* lea 0x0(,%ecx,2),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x0Cu); // 00 001 100
  bb_emit_byte(M, 0x4du); // 01 001 101
  bb_emit_w32(M, 0x0u);   // This 0 word is needed. 
                          // There is no other addressing mode.

  /* movzwl %cx,%ecx */
  bb_emit_byte(M, 0x0Fu); // 0F B7 /R
  bb_emit_byte(M, 0xB7u);
  bb_emit_byte(M, 0xC9u); // 11 001 001

  /* lea M->hash_table(,%ecx,4),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x0Cu); // 00 001 100
  bb_emit_byte(M, 0x8du); // 10 001 101
  bb_emit_w32(M, (unsigned long)M->hash_table);

#else
  /* lea 0x0(,%ecx,4),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x0Cu); // 00 001 100
  bb_emit_byte(M, 0x8du); // 10 001 101
  bb_emit_w32(M, 0x0u);   // This 0 word is needed. 
                          // There is no other addressing mode.

  /* movzwl %cx,%ecx */
  bb_emit_byte(M, 0x0Fu); // 0F B7 /R
  bb_emit_byte(M, 0xB7u);
  bb_emit_byte(M, 0xC9u); // 11 001 001

  /* lea M->hash_table(,%ecx,2),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x0Cu); // 00 001 100
  bb_emit_byte(M, 0x4du); // 01 001 101
  bb_emit_w32(M, (unsigned long)M->hash_table);
#endif


  //50:	ff e1                	jmp    *%ecx
  bb_emit_byte(M, 0xffu);
  bb_emit_byte(M, 0xe1u);
/*   /\* jmp *M->hash_table(,%ecx,4) *\/ */
/*   bb_emit_byte(M, 0xFFu); // FF /4 */
/*   bb_emit_byte(M, 0x24u); // 00 100 100 */
/*   bb_emit_byte(M, 0x8Du); // 10 001 101 */
/*   bb_emit_w32(M, (unsigned long) M->hash_table); */
}

#ifdef SIEVE_WITHOUT_PPF

INLINE void
bb_setup_fast_dispatch_bb(machine_t *M)
{
  /* Emit the special BB that transfers control from
     one basic-block to another within the basic block cache. */

  /* push %ecx */
  bb_emit_byte(M, 0x51u);

  /* mov 0x4(%esp),%ecx */
  bb_emit_byte(M, 0x8bu); // 8b /r
  bb_emit_byte(M, 0x4cu); // 01 001 100
  bb_emit_byte(M, 0x24u); // 00 100 100
  bb_emit_byte(M, 0x4u);

#ifndef SMALL_HASH
  /* lea 0x0(,%ecx,2),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x0Cu); // 00 001 100
  bb_emit_byte(M, 0x4du); // 01 001 101
  bb_emit_w32(M, 0x0u);   // This 0 word is needed. 
                          // There is no other addressing mode.

  /* movzwl %cx,%ecx */
  bb_emit_byte(M, 0x0Fu); // 0F B7 /R
  bb_emit_byte(M, 0xB7u);
  bb_emit_byte(M, 0xC9u); // 11 001 001

  /* lea M->hash_table(,%ecx,4),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x0Cu); // 00 001 100
  bb_emit_byte(M, 0x8du); // 10 001 101
  bb_emit_w32(M, (unsigned long)M->hash_table);

#else
  /* lea 0x0(,%ecx,4),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x0Cu); // 00 001 100
  bb_emit_byte(M, 0x8du); // 10 001 101
  bb_emit_w32(M, 0x0u);   // This 0 word is needed. 
                          // There is no other addressing mode.

  /* movzwl %cx,%ecx */
  bb_emit_byte(M, 0x0Fu); // 0F B7 /R
  bb_emit_byte(M, 0xB7u);
  bb_emit_byte(M, 0xC9u); // 11 001 001

  /* lea M->hash_table(,%ecx,2),%ecx */
  bb_emit_byte(M, 0x8Du); // 8D /r
  bb_emit_byte(M, 0x0Cu); // 00 001 100
  bb_emit_byte(M, 0x4du); // 01 001 101
  bb_emit_w32(M, (unsigned long)M->hash_table);
#endif


  //50:	ff e1                	jmp    *%ecx
  bb_emit_byte(M, 0xffu);
  bb_emit_byte(M, 0xe1u);
/*   /\* jmp *M->hash_table(,%ecx,4) *\/ */
/*   bb_emit_byte(M, 0xFFu); // FF /4 */
/*   bb_emit_byte(M, 0x24u); // 00 100 100 */
/*   bb_emit_byte(M, 0x8Du); // 10 001 101 */
/*   bb_emit_w32(M, (unsigned long) M->hash_table); */
}

#else

INLINE void
bb_setup_fast_dispatch_bb(machine_t *M)
{
  /* Emit the special BB that transfers control from
     one basic-block to another within the basic block cache. */
  /* 0. PUSHF */
  bb_emit_byte (M, 0x9Cu);

  /* Pushl 4(%esp) */
  bb_emit_byte(M, 0xFFu); // FF /6
  bb_emit_byte(M, 0x74u); // 01 110 100
  bb_emit_byte(M, 0x24u); // 00 100 100
  bb_emit_byte(M, 0x4u);
  
  /* andl $(SIEVE_HASH_MASK), (%esp) */
  bb_emit_byte(M, 0x81u); // 81/4
  bb_emit_byte(M, 0x24u); // 00 100 100
  bb_emit_byte(M, 0x24u); // 00 100 100
  bb_emit_w32(M, SIEVE_HASH_MASK);

  /* add $(M->hash_table), (%esp) */
  bb_emit_byte(M, 0x81u); // 81/0
  bb_emit_byte(M, 0x04u); // 00 000 100
  bb_emit_byte(M, 0x24u); // 00 100 100
  bb_emit_w32(M, (unsigned long) M->hash_table);

  /* ret */
  bb_emit_byte(M, 0xC3u);
}

#endif /* SIEVE_WITHOUT_PPF */
#endif /*  USE_SIEVE  */

static void
bb_cache_init(machine_t *M)
{
  M->xlate_bb = (unsigned char *)xlate_bb;
  M->emit_sieve_header_if_bb_present_or_else_xlate_bb = (unsigned char *)emit_sieve_header_if_bb_present_or_else_xlate_bb;

#ifdef SEPARATE_SIEVES
  M->emit_csieve_header_xlate_bb = (unsigned char *)emit_csieve_header_xlate_bb;
#endif
#ifdef PROFILE_BB_STATS
  bb_entry *curr_bb_entry;
#endif
  int i;

  M->bbOut = M->bbCache;
  M->bbLimit = M->bbCache + BBCACHE_SIZE;

#ifdef USE_SIEVE
#ifdef SEPARATE_SIEVES
  M->slow_dispatch_bb = M->bbOut + (NBUCKETS * sizeof(bucket_entry))
    + (CNBUCKETS * sizeof(bucket_entry));
#else
  M->slow_dispatch_bb = M->bbOut + (NBUCKETS * sizeof(bucket_entry));
#endif  

#ifdef SEPARATE_SIEVES
  /*** WARNING: sensitive to size of SLOW_DISPATCH_BB ***/
  M->cslow_dispatch_bb = M->slow_dispatch_bb + 28;
#endif

  /* Set up Sieve */
  M->hash_table = M->bbOut;
  bb_setup_hash_table(M);
#ifdef SEPARATE_SIEVES
  /* Set up Call Sieve */
  M->chash_table = M->bbOut;
  bb_setup_chash_table(M);
#endif
#else /* USE_SIEVE */
  M->slow_dispatch_bb = M->bbOut;
#endif /* USE_SIEVE */

  /* Set up BB-Directory */
  M->no_of_bbs = 0; 
  for (i=0 ; i<LOOKUP_TABLE_SIZE ; i++)
    M->lookup_table[i] = NULL;
 
  /* BB 0 */ 
#ifdef PROFILE_BB_STATS
  curr_bb_entry = make_bb_entry(M, 0, (unsigned long) M->bbOut, (unsigned long) COLD_PROC_ENTRY);
  curr_bb_entry->flags = IS_HAND_CONSTRUCTED | IS_START_OF_TRACE | IS_END_OF_TRACE | NEEDS_RELOC;
#endif
  bb_setup_slow_dispatch_bb(M);

#ifdef SEPARATE_SIEVES
  /* BB 0 */ 
#ifdef PROFILE_BB_STATS
  curr_bb_entry->src_bb_end_eip = 0;
  curr_bb_entry->trans_bb_end_eip = (unsigned long)M->bbOut;
  curr_bb_entry = make_bb_entry(M, 0, (unsigned long) M->bbOut, (unsigned long) COLD_PROC_ENTRY);
  curr_bb_entry->flags = IS_HAND_CONSTRUCTED | IS_START_OF_TRACE | IS_END_OF_TRACE | NEEDS_RELOC;
#endif
  bb_setup_cslow_dispatch_bb(M);
#endif /* SEPARATE_SIEVES */

  /* BB 1 */ 
#ifdef PROFILE_BB_STATS
  curr_bb_entry->src_bb_end_eip = 0;
  curr_bb_entry->trans_bb_end_eip = (unsigned long)M->bbOut;
  curr_bb_entry = make_bb_entry(M, 0, (unsigned long) M->bbOut, (unsigned long) COLD_PROC_ENTRY);
  curr_bb_entry->flags = IS_HAND_CONSTRUCTED | IS_START_OF_TRACE | IS_END_OF_TRACE | NEEDS_RELOC;
#endif
  M->backpatch_and_dispatch_bb = M->bbOut;
  bb_setup_backpatch_and_dispatch_bb(M);

#ifdef USE_SIEVE
  /* BB 2 */ 
#ifdef PROFILE_BB_STATS
  curr_bb_entry->src_bb_end_eip = 0;
  curr_bb_entry->trans_bb_end_eip = (unsigned long)M->bbOut;
  curr_bb_entry = make_bb_entry(M, 0, (unsigned long) M->bbOut, (unsigned long) COLD_PROC_ENTRY);
  curr_bb_entry->flags = IS_HAND_CONSTRUCTED | IS_START_OF_TRACE | IS_END_OF_TRACE | NEEDS_RELOC;
#endif
  M->fast_dispatch_bb = M->bbOut;
  bb_setup_fast_dispatch_bb(M);

#ifdef SEPARATE_SIEVES
#ifdef PROFILE_BB_STATS
  curr_bb_entry->src_bb_end_eip = 0;
  curr_bb_entry->trans_bb_end_eip = (unsigned long)M->bbOut;
  curr_bb_entry = make_bb_entry(M, 0, (unsigned long) M->bbOut, (unsigned long) COLD_PROC_ENTRY);
  curr_bb_entry->flags = IS_HAND_CONSTRUCTED | IS_START_OF_TRACE | IS_END_OF_TRACE | NEEDS_RELOC;
#endif
  M->cfast_dispatch_bb = M->bbOut;
  bb_setup_cfast_dispatch_bb(M);
#endif /* SEPARATE_SIEVES */

#ifdef PROFILE_RET_MISS
  /* BB 3 */
#ifdef PROFILE_BB_STATS
  curr_bb_entry->src_bb_end_eip = 0;
  curr_bb_entry->trans_bb_end_eip = (unsigned long)M->bbOut;
  curr_bb_entry = make_bb_entry(M, 0, (unsigned long) M->bbOut, (unsigned long) COLD_PROC_ENTRY);
  curr_bb_entry->flags = IS_HAND_CONSTRUCTED | IS_START_OF_TRACE | IS_END_OF_TRACE | NEEDS_RELOC;
#endif
  M->call_calls_fast_dispatch_bb = M->bbOut;
  bb_setup_call_calls_fast_dispatch_bb(M);

  /* BB 3 */
#ifdef PROFILE_BB_STATS
  curr_bb_entry->src_bb_end_eip = 0;
  curr_bb_entry->trans_bb_end_eip = (unsigned long)M->bbOut;
  curr_bb_entry = make_bb_entry(M, 0, (unsigned long) M->bbOut, (unsigned long) COLD_PROC_ENTRY);
  curr_bb_entry->flags = IS_HAND_CONSTRUCTED | IS_START_OF_TRACE | IS_END_OF_TRACE | NEEDS_RELOC;
#endif
  M->ret_calls_fast_dispatch_bb = M->bbOut;
  bb_setup_ret_calls_fast_dispatch_bb(M);
#else  /* PROFILE_RET_MISS */
#ifdef SIEVE_WITHOUT_PPF
  M->call_calls_fast_dispatch_bb = M->fast_dispatch_bb + 1;  // Past the push %ecx
#else
  M->call_calls_fast_dispatch_bb = M->fast_dispatch_bb; 
#endif
  M->ret_calls_fast_dispatch_bb = M->fast_dispatch_bb;
#endif /* PROFILE_RET_MISS */
#else /* USE_SIEVE */
  M->call_calls_fast_dispatch_bb = M->slow_dispatch_bb;
  M->ret_calls_fast_dispatch_bb = M->slow_dispatch_bb;
  M->fast_dispatch_bb = M->slow_dispatch_bb;
#endif /* USE_SIEVE */

  /* BB 4 */ 
#ifdef PROFILE_BB_STATS
  curr_bb_entry->src_bb_end_eip = 0;
  curr_bb_entry->trans_bb_end_eip = (unsigned long)M->bbOut;
  curr_bb_entry = make_bb_entry(M, 0, (unsigned long) M->bbOut, (unsigned long) COLD_PROC_ENTRY);
  curr_bb_entry->flags = IS_HAND_CONSTRUCTED | IS_START_OF_TRACE | IS_END_OF_TRACE | NEEDS_RELOC;
#endif
  M->startup_slow_dispatch_bb = M->bbOut;
  bb_setup_startup_slow_dispatch_bb(M);

#ifdef PROFILE_BB_STATS
  curr_bb_entry->src_bb_end_eip = 0;
  curr_bb_entry->trans_bb_end_eip = (unsigned long)M->bbOut;
#endif
  M->bbCache_main = M->bbOut;
 
  for(i=0; i<CALL_TABLE_SIZE; i++)
    M->call_hash_table[i] = (unsigned long) M->ret_calls_fast_dispatch_bb;
}

static void
bb_cache_reinit(machine_t *M)
{
  int i;

#ifdef USE_SIEVE
  M->bbOut = M->hash_table;
  bb_setup_hash_table(M);
#endif

  for (i=0 ; i<LOOKUP_TABLE_SIZE ; i++)
    M->lookup_table[i] = NULL;

  M->bbOut = M->bbCache_main;
  M->no_of_bbs = 0;
  for(i=0; i<CALL_TABLE_SIZE; i++)
    M->call_hash_table[i] = (unsigned long) M->fast_dispatch_bb;
}

#if 0
static void
bb_cache_adjust(machine_t *M)
{
  int i;
  
  M->xlate_bb = (unsigned char *)xlate_bb;
  M->emit_sieve_header_if_bb_present_or_else_xlate_bb = (unsigned char *)emit_sieve_header_if_bb_present_or_else_xlate_bb;
  M->emit_csieve_header_xlate_bb = (unsigned char *)emit_csieve_header_xlate_bb;
  
  M->bbOut = M->bbCache;
  M->bbLimit = M->bbCache + BBCACHE_SIZE;
  
  M->slow_dispatch_bb = M->bbOut + (NBUCKETS * sizeof(bucket_entry));
  /*
    M->hash_table = M->bbOut;
    bb_setup_hash_table(M);
  */
  M->bbOut = M->slow_dispatch_bb;
  bb_setup_slow_dispatch_bb(M);

  M->backpatch_and_dispatch_bb = M->bbOut;
  bb_setup_backpatch_and_dispatch_bb(M);

  M->fast_dispatch_bb = M->bbOut;
  bb_setup_fast_dispatch_bb(M);
  
  M->call_calls_fast_dispatch_bb = M->bbOut;
  bb_setup_call_calls_fast_dispatch_bb(M);

  M->startup_slow_dispatch_bb = M->bbOut;
  bb_setup_startup_slow_dispatch_bb(M);
  
  M->bbCache_main = M->bbOut;
 
  for(i=0; i<CALL_TABLE_SIZE; i++)
    M->call_hash_table[i] = (unsigned long) M->fast_dispatch_bb; 
}
#endif



INLINE bool 
translate_instr(machine_t *M, decode_t *ds)
{

#ifdef INLINE_EMITTERS
  if ((void *)ds->emitfn == (void *)emit_normal){
    DEBUG(inline_emits)
      printf("Tanslation Path - inline_emit_normal\n");
    return inline_emit_normal(M, ds);
  }
  else 	 
#endif /* INLINE_EMITTERS */
    {
      DEBUG(inline_emits)
	printf("Tanslation Path - NOT inline_emit_normal\n");

      /* DBT-GPU */
      if (emit_switch) {
        //printf("%d %s\n", __LINE__, __FILE__);
        printf("ds: %x, emitfn: %x\n", ds, ds->emitfn);
      }

      return (ds->emitfn)(M, ds);
    }
}

#ifdef STATIC_PASS  
bool 
update_mem_next_eip(machine_t *M)
{
  int i;
  i = M->curr_sec_index;
  bool found = false;

  /* Usually in the same section */
  if((M->sec_info[i].start <= M->next_eip) && (M->sec_info[i].end >= M->next_eip)) {
    found = true;
  }
  else {
    /* If not, find it the hard way */
    for(i=0;(!found) && (i<M->nsections); i++) 
      if((M->sec_info[i].start <= M->next_eip) && (M->sec_info[i].end >= M->next_eip)) {
	found = true;
	break;
      }
  }

  if(found) {
    DEBUG(static_pass_addr_trans)
      printf("\t#%lx: [%s]\n",M->next_eip, M->sec_info[i].name);
    M->mem_next_eip = (unsigned long) (M->sec_info[i].inmem + 
				       (M->next_eip - (unsigned long)M->sec_info[i].start));
    M->curr_sec_index = i;
  }
  else {
    DEBUG(static_pass_addr_trans)
      printf("\t@%lx: Not Found\n",M->next_eip);  
  }
  fflush(stdout);
  return !found;
}

void
simple_patch(machine_t *M, unsigned long at, unsigned long addr)
{
  unsigned char *tmp = M->bbOut;
  M->bbOut = (unsigned char *)at;
  bb_emit_w32(M, addr - (at + 4));
  	M->bbOut = tmp;
}
#endif  

#define PATCH_BLOCK_LEN 13 /* WARNING: Sensitive to size of Patch-block */
#define MAX_PATCH_BLOCK_BYTES (PATCH_ARRAY_LEN * PATCH_BLOCK_LEN)

/* Conservative estimate used to determine if there is more room for this BB.
   Space needed for all patch_blocks + space for at least one instruction 
   I guess no emitted sequence of instructions per single instruction currently
   exceeds 64 bytes. If it does, fix the next line */
#define BYTES_NEEDED_AT_THE_END (MAX_PATCH_BLOCK_BYTES + 64)

#define ROOM_FOR_BB(M) ((M->bbLimit - M->bbOut) > BYTES_NEEDED_AT_THE_END)
#define MORE_FREE_PATCH_BLOCKS(M) (M->patch_count <= (PATCH_ARRAY_LEN - 4))  /* Leave some extra room for cases like
										call that need multiple patch_blocks */

/* THE Translator -- Returns:
   - a pointer to the bb_entry of the required destination
   - M->jmp_target holds the bb address of the destunation
   (There is no peculiar reson to have this convention, it is just a 
   hack to avoid emitting code to get the jmp destination from the
   bb_entry. The translator has to be locked prior to the backpatcher
   anyway, so this does not present any new problems for locking */

bb_entry * 
xlate_bb(machine_t *M)
{
  decode_t ds;				  /* Decode Structure - filled up by do_decode and used by translate_instr   	*/
  bool isEndOfBB = true;		  /* Flag to indicate encountering of basic-block terminating instruction    	*/
  bool goingOutofElf = true;
  unsigned long bbStart, start_eip;
  int i, j;
  unsigned char * tmp;
  bb_entry *curr_bb_entry = lookup_bb_eip(M, M->fixregs.eip), *temp_entry;

  /* If the required bb is already found, just return */
  if(curr_bb_entry != NULL) {
	
    DEBUG(xlate) {
      printf("Entering New translation in Proc %lx\n",curr_bb_entry->proc_entry);
      fflush(stdout);
    }

    if(curr_bb_entry->trans_bb_eip != NOT_YET_TRANSLATED) {
      M->jmp_target = (unsigned char *)curr_bb_entry->trans_bb_eip;
      return curr_bb_entry;
    }
    else
      curr_bb_entry->trans_bb_eip = (unsigned long)M->bbOut;
  }
  else {

    if (M->comming_from_call_indirect) {
      curr_bb_entry = make_bb_entry(M, M->fixregs.eip, (unsigned long) M->bbOut, 
				    (unsigned long) CALL_HASH_BUCKET(M->call_hash_table, M->fixregs.eip));
    }
    else {
      curr_bb_entry = make_bb_entry(M, M->fixregs.eip, (unsigned long) M->bbOut,
				    (unsigned long) COLD_PROC_ENTRY);
    }
    DEBUG(xlate) {
      printf("Entering New translation $#COLD$# on %lx::%lx\n",M->fixregs.eip, 
	     curr_bb_entry->trans_bb_eip);
      fflush(stdout);
    }
  }
#ifdef DEBUG_ON
  M->nTrInstr = 0;
#endif

  /* Did not find the translated Basic Block, So need to Translate */

  M->comming_from_call_indirect = false;
  M->curr_bb_entry = curr_bb_entry;

#if 0
  struct bb_mapping_entry {
    unsigned short src_eip_offset;
    unsigned short bb_eip_offset;
  } bb_mappings[MAX_TRACE_INSTRS];	     /* src_eip_offset to bb_eip_offset
						mappings array that will be filled up 
						after translating each instruction */
#endif
 

  bbStart = (unsigned long) M->bbOut;
  start_eip = M->next_eip = M->fixregs.eip;

  M->patch_count = 0;

  /* If basic block cache is full, wipe it and start over. */
  /* This is not done inside the loop on purpose. Dont wipe the BBCache
     Unless utterly necessary. We translate as far as possible and return 
     with the hope that the guest executes upto completion without need for 
     for translation */
  if ((!ROOM_FOR_BB(M)) || (M->no_of_bbs >= MAX_BBS)) {
    printf("Wiping basic block cache\n");
    bb_cache_reinit(M);
  }

#ifdef PROFILE_BB_STATS
  bb_entry *this_bb_entry = M->curr_bb_entry;
  this_bb_entry->flags = IS_START_OF_TRACE;
  this_bb_entry->src_bb_end_eip = M->next_eip;
  this_bb_entry->trans_bb_end_eip = (unsigned long) M->bbOut;
#endif



  /* DBT-GPU */
  set_bbOut(M->bbOut);
  set_next_eip(M->next_eip);
  set_curr_bb_entry(M->curr_bb_entry);
  emit_switch = false;
  clear_ds();

  /* This loop executes once per instruction */  
  while (ROOM_FOR_BB(M) && MORE_FREE_PATCH_BLOCKS(M)) {
    
    /*If it is necessary to limit the trace length (I don't know why)
      use : M->nTrInstr < MAX_TRACE_INSTRS */
    
    /* See if the instruction needs to be decoded */
#ifdef STATIC_PASS
    goingOutofElf = update_mem_next_eip(M); /* Jump to a library etc, outside of the 
					       current elf file */
    if (goingOutofElf)
      break;
#endif
    
    /* Decode the Instruction */
#ifdef STATIC_PASS
    bool what_happened = do_decode(M, &ds);
    if (what_happened == false) {
      /* Emit a call to panic() */
      //printf("Decode failed\n");
      break;
    }
#else



    if (do_decode(M, &ds) == false) {
      panic ("do_decode failed");
      break;
    }

    /* DBT-GPU */
    if (!enqueue_ds(&ds)) {
      panic("decode queue overflowed!\n");
      exit(1);
    }



#endif

    DEBUG(show_each_instr_trans) {
      printf("bb# %lu, %lx: ", M->no_of_bbs, M->bbOut);
      fflush(stdout);
      do_disasm(&ds, stdout);
    }

    unsigned char *begin_bbout = M->bbOut;
#ifdef DEBUG_ON
    M->nTrInstr++;
#endif

#ifdef PROFILE_BB_STATS
  this_bb_entry->nInstr++;
    /* Note:  this_bb_entry->src_bb_end_eip MUST be updated before 
       emitting. This is because, some emitters do change M->next_eip */
  this_bb_entry->src_bb_end_eip = M->next_eip;
#endif 

  /* Emit the Instruction using the appropriate emitter */
  isEndOfBB = translate_instr(M, &ds);

  DEBUG(show_each_trans_instr) {
    unsigned long saved_Meip = M->next_eip;
    M->next_eip = (unsigned long)begin_bbout;
    while(M->next_eip < (unsigned long)M->bbOut) {
      decode_t dd;
      do_decode(M, &dd);
      do_disasm(&dd, stdout);
      fflush(stdout);
    }
    M->next_eip = saved_Meip;
    printf("\n");
  }
  
#ifdef PROFILE_BB_STATS
    /* Note:  this_bb_entry->trans_bb_end_eip MUST be updated after 
       emitting, for obvious reasons. */
  this_bb_entry->trans_bb_end_eip = (unsigned long) M->bbOut;
  if(this_bb_entry != M->curr_bb_entry) {
    this_bb_entry->trace_next = M->curr_bb_entry;
    this_bb_entry = M->curr_bb_entry;
  }
#endif
 
    if (isEndOfBB)
      break;
  } /* Grand Translation Loop */



#if 1
  printf("Optimus!!\n");
  
  /* DBT-GPU */
  M->bbOut = get_bbOut(); 
  M->next_eip = get_next_eip();
  M->curr_bb_entry = get_curr_bb_entry();
  emit_switch = true;

  /* This loop executes once per instruction */
  while (ROOM_FOR_BB(M) && MORE_FREE_PATCH_BLOCKS(M)) {

    decode_t* ds_tmp = dequeue_ds();
    
  printf("%d:%s\n", __LINE__, __FILE__);

    if (ds_tmp) {
      /* Emit the Instruction using the appropriate emitter */
      isEndOfBB = translate_instr(M, ds_tmp);
    }
    else {
      isEndOfBB = true;
    }

    if (isEndOfBB)
      break;
  }
    //printf("%d %s\n", __LINE__, __FILE__);
#endif

 
  if (!isEndOfBB) {
    //printf("%d %s\n", __LINE__, __FILE__);
    if(M->curr_bb_entry->trans_bb_eip == (unsigned long)M->bbOut)
      M->curr_bb_entry->trans_bb_eip = NOT_YET_TRANSLATED;
    bb_emit_jump(M, 0);
    M->patch_array[M->patch_count].at = M->bbOut - 4;
    M->patch_array[M->patch_count].to = (unsigned char *)M->next_eip;
    M->patch_array[M->patch_count].proc_addr = M->curr_bb_entry->proc_entry;
    M->patch_count ++;
  }
#ifdef PROFILE_BB_STATS
  else {
    //printf("%d %s\n", __LINE__, __FILE__);
    M->curr_bb_entry->flags |= IS_END_OF_TRACE;
    M->curr_bb_entry->trace_next = NULL;
  }
#endif

#ifdef STATIC_PASS
  /* I need not emit Patch blocks when statically translating. The
     driver will next add these items to the worklist, and will
     arrange for back-patching
     
     However, If I am going out of the current binary, 
     I must emit a patch block. hence the following ...   */

  if(goingOutofElf) {
    //printf("%d %s\n", __LINE__, __FILE__);
    bb_entry *entry = lookup_bb_eip (M, M->next_eip);
    bb_emit_byte (M, 0xE8u);	/* CALL rel32 */
    bb_emit_w32 (M, (unsigned long) (M->backpatch_and_dispatch_bb - (unsigned long)(M->bbOut + 4)));
    bb_emit_w32 (M, M->next_eip);
    bb_emit_w32 (M, (unsigned long)M->bbOut - 4);
    if(entry == NULL) {
  printf("%d %s\n", __LINE__, __FILE__);
      temp_entry = make_bb_entry(M, M->next_eip, NOT_YET_TRANSLATED, M->curr_bb_entry->proc_entry);
    }
  }
#else
  /* Lastly, emit the Patch Blocks */
  for (i = 0 ; i < M->patch_count ; i ++) {
    //printf("%d %s\n", __LINE__, __FILE__);
    bb_entry *entry = NULL; 
    unsigned long addr;
    unsigned long to = (unsigned long)M->patch_array[i].to;
    unsigned long at = (unsigned long)M->patch_array[i].at;

    entry = lookup_bb_eip (M, to);
    if ((entry != NULL) && (entry->trans_bb_eip != NOT_YET_TRANSLATED)) {
      //printf("%d %s\n", __LINE__, __FILE__);
      //		if (entry != NULL)
      DEBUG(xlate_pb) {
	printf("1. Patch Block: BB Already found\n");
	fflush(stdout);
      }
      addr = entry->trans_bb_eip;
      /* If found to be translated already, patch the jump destination right now to implement chaining */
      tmp = M->bbOut;
      M->bbOut = (unsigned char *)at;
      bb_emit_w32(M, addr - (at + 4));
      M->bbOut = tmp;
    }
    else {
    //printf("%d %s\n", __LINE__, __FILE__);
      /* If not, patch the jump destination so that it jumps to its corresponding patch block */
      tmp = M->bbOut;
      M->bbOut = (unsigned char *)at;
      bb_emit_w32(M, tmp - (M->bbOut + 4));
      M->bbOut = tmp;
      
      /* Then, build the patch block */
      bb_emit_byte (M, 0xE8u);	/* CALL rel32 */
      bb_emit_w32 (M, (unsigned long) (M->backpatch_and_dispatch_bb - (unsigned long)(M->bbOut + 4)));
      bb_emit_w32 (M, to);
      bb_emit_w32 (M, at);
      if(entry == NULL) {
	temp_entry = make_bb_entry(M, to, NOT_YET_TRANSLATED, M->patch_array[i].proc_addr);
      }
    }
  }
#endif
  M->jmp_target = (unsigned char *)bbStart; 
  //return (lookup_bb_eip(M, (unsigned long)M->fixregs.eip));
  return(curr_bb_entry);;   
}

machine_t *MM;

void 
handle_sigsegv(int param, struct sigcontext ctx)
{
  struct sigcontext *context = &ctx;
  bb_entry *curr;
  int i;
  machine_t *M = MM;

  printf("\nVDebug is handling SIGSEGV...\n");
	
  printf("eip: %08X\n", context->eip);
  fflush(stdout);
  
  fflush(stdout);
#if 0   
  for (i=0 ; i<LOOKUP_TABLE_SIZE ; i++) {
    curr =  M->lookup_table[i];
    while ((curr != NULL) && 
	   ((context->eip < curr->trans_bb_eip) || (context->eip >= curr->trans_bb_eip + curr->trans_bb_size)))
      curr = curr->next;
    if (curr != NULL) {
      int j;
      printf ("\nFound src_bb_start_eip to be: %08X\n", curr->src_bb_eip);
      printf ("\ntrans_bb_start_eip is: %08X\n", curr->trans_bb_eip);
      printf ("\nAnd, the Basic Block is as follows: \n");
      for (j=0 ; j<curr->trans_bb_size ; j++)
	printf ("%02X  ", *((unsigned char *)curr->trans_bb_eip + j));
      printf ("\n");
    }
  }
  fflush(stdout);
#endif

  M->next_eip = (unsigned long)M->slow_dispatch_bb;
  while(M->next_eip < (unsigned long)M->backpatch_and_dispatch_bb) {
    decode_t dd;
    do_decode(M, &dd);
    do_disasm(&dd, stdout);
    fflush(stdout);
  }
  

  M->next_eip = (unsigned long)M->fast_dispatch_bb;
  while(M->next_eip < (unsigned long)M->startup_slow_dispatch_bb) {
    decode_t dd;
    do_decode(M, &dd);
    do_disasm(&dd, stdout);
    fflush(stdout);
  }
  printf("\n");
  exit(1);
}

#ifndef USE_STATIC_DUMP
machine_t theMachine;
#endif

#ifdef TOUCH_RELOADED_BBCACHE_PAGES
void
touch_pages(machine_t *M)
{
  size_t n;
  for(n=0; n<450; n++) {
    size_t ndx = n*PAGE_SIZE + 1;
    volatile val;
    val = ((unsigned long *)M->bbCache)[ndx];
    ((unsigned long *)M->bbCache)[ndx] = val;
  }
}
#endif /* TOUCH_RELOADED_BBCACHE_PAGES */



machine_t * 
init_translator(unsigned long program_start)
{
  machine_t *M;
  int i;
  bb_entry *temp_entry;

  DEBUG(startup) {
    printf("VDebug rules!... \n");
    printf("Initial eip   = %lx\n", program_start);
  }

#ifndef USE_STATIC_DUMP
  M = &theMachine;

  bb_cache_init(M);
  temp_entry = make_bb_entry(M, program_start, NOT_YET_TRANSLATED, 
			     CALL_HASH_BUCKET(M->call_hash_table, program_start));
#ifdef PROFILE_BB_STATS
  temp_entry->flags = 0;
#endif

#else /* USE_STATIC_DUMP */

  char str[300];
  char arg[300];
  sprintf(arg, "/proc/%d/exe", getpid());
  i = readlink(arg,str,sizeof(str));
  str[i] = '\0';
  for(i--; i>=0 ; i--)
    if(str[i] == '/')
      str[i] = '_';
  
  sprintf(arg, "/tmp/vdebug-dump/%s-dump",str);
  int fd1 = open(arg, O_RDONLY, S_IRWXU);

  sprintf(arg, "/tmp/vdebug-dump/%s-addr",str);
  int fd2 = open(arg, O_RDONLY, S_IRWXU);

  if(fd1 == -1 || fd2 == -1) {
    M = (machine_t *) mmap(0, sizeof(machine_t), 
			   PROT_READ | PROT_WRITE | PROT_EXEC,
			   MAP_ANONYMOUS | MAP_PRIVATE | MAP_NORESERVE, 0, 0);

    if(M == MAP_FAILED)
      panic("Allocation of M failed");

    fflush(stdout);

    bb_cache_init(M);
    temp_entry = make_bb_entry(M, program_start, NOT_YET_TRANSLATED, 
			       CALL_HASH_BUCKET(M->call_hash_table, program_start));
#ifdef PROFILE_BB_STATS
    temp_entry->flags = 0;
#endif
    M->dump = true;
  }
  else {
    machine_t *maddr;
    read(fd2, &maddr, sizeof(unsigned char *));
    close(fd2);

    DEBUG(dump_load)
      printf("Came to File-open area of initializer\n");
    
    M = (machine_t *) mmap(maddr, sizeof(machine_t), 
			   PROT_READ | PROT_WRITE | PROT_EXEC,
			   MAP_FIXED | MAP_PRIVATE | MAP_NORESERVE, fd1, 0);
    if(M == MAP_FAILED)
      panic("Allocation of M at %lx failed", maddr);
    close(fd1);
    M->dump = false;

#ifdef TOUCH_RELOADED_BBCACHE_PAGES
    printf("%lu, %lu\n", (M->bbOut - M->bbCache)/PAGE_SIZE, BBCACHE_SIZE/PAGE_SIZE);
    fflush(stdout);
    touch_pages(M);
#endif /* TOUCH_RELOADED_BBCACHE_PAGES */
  }
#endif /* USE_STATIC_DUMP main */
  
  
  M->fixregs.eip = program_start;
  M->guest_start_eip = program_start;
  M->comming_from_call_indirect = false;

#ifdef DEBUG_ON
  M->nTrInstr = 0;
#endif

#ifdef PROFILE
  M->total_cnt = 0;
  M->normal_cnt = 0; 
  M->ret_cnt = 0;
  M->ret_Iw_cnt = 0;
  M->call_dir_cnt = 0;
  M->call_indr_cnt = 0;
  M->jmp_indr_cnt = 0;
  M->jmp_dir_cnt = 0;
  M->jmp_cond_cnt = 0;

  M->s_total_cnt = 0;
  M->s_normal_cnt = 0; 
  M->s_ret_cnt = 0;
  M->s_ret_Iw_cnt = 0;
  M->s_call_dir_cnt = 0;
  M->s_call_indr_cnt = 0;
  M->s_jmp_indr_cnt = 0;
  M->s_jmp_dir_cnt = 0;
  M->s_jmp_cond_cnt = 0;

  M->hash_nodes_cnt = 0;
  M->max_nodes_trav_cnt = 0;
#endif
 
#ifdef PROFILE_RET_MISS  
  M->ret_miss_cnt = 0;
  M->ret_ret_miss_cnt = 0;
  M->cold_cnt = 0;
#endif

  DEBUG(sigsegv) {
    MM = M;
    signal(SIGSEGV, (void *)handle_sigsegv);
  }
  
  DEBUG(ret_cache) {
    DBG = fopen("DBG", "w");
    fprintf(DBG, "Call hash Table start = %lx\n", M->call_hash_table);
  }

  return M;
}
 
