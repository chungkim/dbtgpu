	.file "UserEntry.s"
	
.section .init
	call UserEntry
	
.section .text
	.align 2
.globl UserEntry
	.type	UserEntry,@function
	
UserEntry:
	subl $32,%esp	/* subtract 32 from %esp.*/
	pushl 32(%esp)	/* push 32 bytes from %esp. (32 bytes backed up at lower address.) */
	add $40,%esp	/* add 40 to %esp. (%esp moved to -8.) */

	pushf	/* push eflags to stack. (%esp moved to -4.) */
	pusha	/* push register values to stack. (%esp moved to 0.) */

	subl 	$4,%esp		/* add 4 to %esp (parameter set to target start address.) */
	call 	init_translator	/* let the translator init itself. */
	addl	$4,%esp		/* restore %esp back. */
	
	movl 	(%eax),%eax	/* return value (M->startup_slow_dispatch_bb) set to %eax */
	jmpl	*%eax		/* jump to 'M->startup_slow_dispatch_bb'. */

.Lfe1:
	.size	UserEntry,.Lfe1-UserEntry
