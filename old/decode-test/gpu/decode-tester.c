#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include "switches.h"
#include "debug.h"
#include "machine.h"
#include "decode.h"
#include "emit.h"
#include "util.h"

#include "defs.h"

extern int kernel(machine_t* M, decode_t* D, int N);

machine_t IA32;
char instr_stream[] = {0xf2, 0x0f, 0x5c, 0xcd,
		       //0xc3, 0xc3, 0xc3, 0xc3,
		       0x66, 0x0f, 0x7e, 0xc2, 
		       0x0};
decode_t D[128] = {0,};

int
main()
{
  machine_t *M = &IA32;
  decode_t ds;
  decode_t *d = &ds;
  M->next_eip = (unsigned long)instr_stream;
  int i = 0; 
#if 1
 
  while(*((unsigned char *) M->next_eip)) {
    do_decode(M, d);
    do_disasm(d, stdout);
  }
  printf("0x%08lx:     THE END\n", M->next_eip);
  
  M->next_eip = (unsigned long)instr_stream;
#endif 

  // let gpu decoder handles it 
  printf("CALLING KERNEL()...\n");
  kernel(M, D, sizeof(instr_stream));
  printf("KERNEL() RETURNED!\n");

#if 0
  // disassemble the decoded instructions
  while (D[i].decode_eip) {
    d = &D[i++];
    do_disasm(d, stdout);
  }
  printf("0x%08lx:     THE END\n", M->next_eip);
#endif 
}
