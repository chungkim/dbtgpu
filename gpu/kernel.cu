// All CUDA specific stuff goes here
//

/* Includes, system */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* Includes, cuda */
#include <cuda.h>
#include <cublas.h>
#include <cutil.h>

/* Inludes, defs */
#include "defs.h"

extern "C" void xlate_gpu(machine_t* m);
extern "C" int emit_normal(unsigned long next_eip, decode_t* d,
                        unsigned char* bbOut, unsigned char* bbLimit);
extern "C" int emit_jmp(unsigned long next_eip, decode_t* d,
                        unsigned char* bbOut, unsigned char* bbLimit);

static inline unsigned long long
read_timer()
{
	unsigned long temp1;
	unsigned long temp2;
	unsigned long long time;
	asm volatile("rdtsc\t\n"
			"movl %%eax, %0\t\n"
			"movl %%edx, %1\t\n"
			: "=m" (temp1), "=m" (temp2)
			:
			: "%eax", "%edx");

	time = (((unsigned long long)temp2) << 32) + temp1;
	return time;
}

__device__ OpCode *nopbyte0_d, *twoByteOpcodes_d; 
__device__ OpCode *group1_Eb_Ib_d, *group1_Ev_Iv_d, *group1_Ev_Ib_d;
__device__ OpCode *group2a_Eb_Ib_d, *group2a_Ev_Ib_d, *group2_Eb_1_d; 
__device__ OpCode *group2_Ev_1_d, *group2_Eb_CL_d, *group2_Ev_CL_d;
__device__ OpCode *group3b_d, *group3v_d;
__device__ OpCode *ngroup4_d, *ngroup5_d, *ngroup6_d, *ngroup7_d, *ngroup9_d;
__device__ OpCode *group8_Ev_Ib_d;
__device__ OpCode *float_d8_d, *float_d9_d, *float_d9_2_d; 
__device__ OpCode *float_d9_4_d, *float_d9_5_d, *float_d9_6_d, *float_d9_7_d;
__device__ OpCode *float_da_d, *float_da_5_d;
__device__ OpCode *float_db_d, *float_db_4_d;
__device__ OpCode *float_dc_d, *float_dd_d, *float_de_d, *float_de_3_d;
__device__ OpCode *float_df_d, *float_df_4_d;

__device__ bool do_decode(unsigned long next_eip, decode_t* ds)
{
    //unsigned i;
    const OpCode *table = nopbyte0_d;
    const OpCode *pEntry = 0;
    //int wierd = 0;

    ds->decode_eip = next_eip;
    ds->attr = 0;
    ds->emitfn = 0;
    ds->modrm_regs = 0;
    //ds->need_sib = 0;
    //ds->dispBytes = 0;
    ds->no_of_prefixes = 0;
    ds->flags = 0;

    for(;;) {
	ds->b = (* ((unsigned char *) next_eip));
	if (ds->b > 255u)
	    goto handle_ifault;

	ds->attr = table[ds->b].attr;

	if ((ds->attr & DF_PREFIX) == 0)
	    break;

	ds->no_of_prefixes ++;

	switch(ds->b) {
	    case PREFIX_LOCK:
	    case PREFIX_REPZ:
	    case PREFIX_REPNZ:
		ds->flags |= DSFL_GROUP1_PREFIX;
		ds->Group1_Prefix = ds->b;
		break;

	    case PREFIX_CS:
	    case PREFIX_DS:
	    case PREFIX_ES:
	    case PREFIX_FS:
	    case PREFIX_GS:
	    case PREFIX_SS:
		ds->flags |= DSFL_GROUP2_PREFIX;
		ds->Group2_Prefix = ds->b;
		break;

	    case PREFIX_OPSZ:
		ds->flags |= DSFL_GROUP3_PREFIX;
		ds->opstate ^= OPSTATE_DATA32;
		ds->Group3_Prefix = ds->b;
		break;

	    case PREFIX_ADDRSZ:
		ds->flags |= DSFL_GROUP4_PREFIX;
		ds->opstate ^= OPSTATE_ADDR32;
		ds->Group4_Prefix = ds->b;
		break;
	}

	(next_eip)++;
    } 

    ds->instr = (unsigned char *)next_eip;

    for(;;) {
	ds->b = (* ((unsigned char *) next_eip));
	if (ds->b > 255u)
	    goto handle_ifault;
	pEntry = &table[ds->b];
	ds->attr = pEntry->attr;
	(next_eip)++;

	if (ds->attr & DF_TABLE) {
	    table = (const OpCode*)pEntry->ptr;
	    //ds->attr &= ~DF_TABLE;
	    continue;
	}

	if (ds->attr & DF_FLOAT) {
	    //wierd = 1;
	    table = (const OpCode*)pEntry->ptr;
	    ds->b = (* ((unsigned char *) next_eip));
	    if (ds->b > 255u)
		goto handle_ifault;
	    (next_eip)++;
	    ds->modrm.byte = ds->b;
	    if (ds->modrm.parts.mod == 0x3u)
	    {
		pEntry = &table[ds->modrm.parts.reg + 8];
		ds->attr = pEntry->attr;
		if (ds->attr & DF_ONE_MORE_LEVEL)
		{
		    table = (const OpCode*)pEntry->ptr;
		    pEntry = &table[ds->modrm.parts.rm];
		    ds->attr = pEntry->attr;
		}
	    }
	    else
	    {
		pEntry = &table[ds->modrm.parts.reg];
		ds->attr = pEntry->attr;
	    }
	}

	break;
    }

    /* a lot of branches */

    ds->pInstr = (unsigned char *)next_eip;
    ds->emitfn = (EMITFN)pEntry->ptr;
    ds->pEntry = (void*)pEntry;

    if (ds->attr & DF_UNDEFINED)
	return false;

    if (pEntry->ptr == 0)
        return false;

    return true;

handle_ifault:
    ds->pInstr = (unsigned char *)next_eip;

    ds->emitfn = (EMITFN)pEntry->ptr;
    ds->pEntry = (void*)pEntry;

    return false;
} 


__device__ bool bb_emit_byte(unsigned char* bbOut, unsigned char* bbLimit, unsigned char c)
{
    if (bbOut == bbLimit)
	return false;
    else {
        *bbOut = c;
        bbOut++;
    }
    return true;
} 

__device__ int emit_normal(unsigned long next_eip, decode_t* d,
		unsigned char* bbOut, unsigned char* bbLimit)
{
    int i;
    //int count = next_eip - (int)d->decode_eip;
    int count = 1;
    for (i = 0; i < count; i++)
    {
        if (!bb_emit_byte(bbOut, bbLimit, *(d->instr)))
	    return 0;
    }
    
    return 1;
}

__device__ int emit_jmp(unsigned long next_eip, decode_t* d, 
	                unsigned char* bbOut, unsigned char* bbLimit)
{
    int i;
    int count = next_eip - (int)d->decode_eip;
    for (i = 0; i < count; i++)
    {
        if (!bb_emit_byte(bbOut, bbLimit, *(d->instr)))
	    return 0;
    }

    return 1;
}

__device__ bool translate_instr(unsigned long next_eip, decode_t* ds, 
		unsigned char* bbOut, unsigned char* bbLimit)
{
    //return (ds->emitfn)(next_eip, ds, bbOut, bbLimit);
    return (emit_normal(next_eip, ds, bbOut, bbLimit));
}


__global__ void xlate_kernel(char* instr_stream, unsigned char* code_cache)
{
    unsigned long next_eip;
    decode_t d;
    
    next_eip = (unsigned long)instr_stream + (blockIdx.x * blockDim.x + threadIdx.x);
    do_decode(next_eip, &d);

    unsigned char* bbOut = code_cache + (blockIdx.x * blockDim.x + threadIdx.x);
    unsigned char* bbLimit = code_cache + CODE_CACHE_SIZE;
    translate_instr(next_eip, &d, bbOut, bbLimit);
}

__host__ void xlate_gpu(machine_t* m)
{
//	OpCode *nopbyte0_d, *twoByteOpcodes_d; 
//	OpCode *group1_Eb_Ib_d, *group1_Ev_Iv_d, *group1_Ev_Ib_d;
//	OpCode *group2a_Eb_Ib_d, *group2a_Ev_Ib_d, *group2_Eb_1_d; 
//	OpCode *group2_Ev_1_d, *group2_Eb_CL_d, *group2_Ev_CL_d;
//	OpCode *group3b_d, *group3v_d;
//	OpCode *ngroup4_d, *ngroup5_d, *ngroup6_d, *ngroup7_d, *ngroup9_d;
//	OpCode *group8_Ev_Ib_d;
//	OpCode *float_d8_d, *float_d9_d, *float_d9_2_d; 
//	OpCode *float_d9_4_d, *float_d9_5_d, *float_d9_6_d, *float_d9_7_d;
//	OpCode *float_da_d, *float_da_5_d;
//	OpCode *float_db_d, *float_db_4_d;
//	OpCode *float_dc_d, *float_dd_d, *float_de_d, *float_de_3_d;
//	OpCode *float_df_d, *float_df_4_d;
	char* instr_stream_d;
	unsigned char* codeCache_d;
	int i;

	unsigned long long start_init = read_timer();
	CUDA_SAFE_CALL(cudaMalloc((void **) &nopbyte0_d, 256*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &twoByteOpcodes_d, 256*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &group1_Eb_Ib_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &group1_Ev_Iv_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &group1_Ev_Ib_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &group2a_Eb_Ib_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &group2a_Ev_Ib_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &group2_Eb_1_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &group2_Ev_1_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &group2_Eb_CL_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &group2_Ev_CL_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &group3b_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &group3v_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &ngroup4_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &ngroup5_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &ngroup6_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &ngroup7_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &ngroup9_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &group8_Ev_Ib_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_d8_d, 16*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_d9_d, 16*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_d9_2_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_d9_4_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_d9_5_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_d9_6_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_d9_7_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_da_d, 16*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_da_5_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_db_d, 16*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_db_4_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_dc_d, 16*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_dd_d, 16*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_de_d, 16*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_de_3_d, 8*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_df_d, 16*sizeof(OpCode))); 
	CUDA_SAFE_CALL(cudaMalloc((void **) &float_df_4_d, 8*sizeof(OpCode))); 
	
	CUDA_SAFE_CALL(cudaMemcpy(nopbyte0_d, nopbyte0, 256*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(twoByteOpcodes_d, twoByteOpcodes, 256*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(group1_Eb_Ib_d, group1_Eb_Ib, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(group1_Ev_Iv_d, group1_Ev_Iv, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(group1_Ev_Ib_d, group1_Ev_Ib, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(group2a_Eb_Ib_d, group2a_Eb_Ib, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(group2a_Ev_Ib_d, group2a_Ev_Ib, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(group2_Eb_1_d, group2_Eb_1, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(group2_Ev_1_d, group2_Ev_1, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(group2_Eb_CL_d, group2_Eb_CL, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(group2_Ev_CL_d, group2_Ev_CL, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(group3b_d, group3b, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(group3v_d, group3v, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(ngroup4_d, ngroup4, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(ngroup5_d, ngroup5, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(ngroup6_d, ngroup6, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(ngroup7_d, ngroup7, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(ngroup9_d, ngroup9, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(group8_Ev_Ib_d, group8_Ev_Ib, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_d8_d, float_d8, 16*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_d9_d, float_d9, 16*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_d9_2_d, float_d9_2, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_d9_4_d, float_d9_4, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_d9_5_d, float_d9_5, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_d9_6_d, float_d9_6, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_d9_7_d, float_d9_7, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_da_d, float_da, 16*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_da_5_d, float_da_5, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_db_d, float_db, 16*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_db_4_d, float_db_4, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_dc_d, float_dc, 16*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_dd_d, float_dd, 16*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_de_d, float_de, 16*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_de_3_d, float_de_3, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_df_d, float_df, 16*sizeof(OpCode), cudaMemcpyHostToDevice)); 
	CUDA_SAFE_CALL(cudaMemcpy(float_df_4_d, float_df_4, 8*sizeof(OpCode), cudaMemcpyHostToDevice)); 

	CUDA_SAFE_CALL(cudaMalloc((void **) &instr_stream_d, INSTR_STREAM_SIZE));
	CUDA_SAFE_CALL(cudaMalloc((void **) &codeCache_d, CODE_CACHE_SIZE));

	unsigned long long end_init = read_timer();

	unsigned long long start_mem = read_timer();

	CUDA_SAFE_CALL(cudaMemcpy(instr_stream_d, (unsigned char*)m->next_eip, INSTR_STREAM_SIZE, cudaMemcpyHostToDevice));
	//CUDA_SAFE_CALL(cudaMemcpy(codeCache_d, m->code_cache, CODE_CACHE_SIZE, cudaMemcpyHostToDevice)); 

	int grid, block;
	block = 512;
	grid = INSTR_STREAM_SIZE / block;

	unsigned long long start = read_timer();

        xlate_kernel <<< grid, block >>> (instr_stream_d, codeCache_d);
	CUDA_SAFE_CALL(cudaThreadSynchronize());

	unsigned long long end = read_timer();

	CUDA_SAFE_CALL(cudaMemcpy(m->code_cache, codeCache_d, CODE_CACHE_SIZE, cudaMemcpyDeviceToHost)); 

	unsigned long long end_mem = read_timer();

	unsigned long long start_cleanup = read_timer();

	CUDA_SAFE_CALL(cudaFree(codeCache_d));
	CUDA_SAFE_CALL(cudaFree(instr_stream_d));

/*
	printf("code cache: \n");
	for (i = 0; i < INSTR_STREAM_SIZE; i++)
	    printf("0x%02x  ", m->code_cache[i]);
	printf("\n");
*/	
	CUDA_SAFE_CALL(cudaFree(nopbyte0_d)); 
	CUDA_SAFE_CALL(cudaFree(twoByteOpcodes_d)); 
	CUDA_SAFE_CALL(cudaFree(group1_Eb_Ib_d)); 
	CUDA_SAFE_CALL(cudaFree(group1_Ev_Iv_d)); 
	CUDA_SAFE_CALL(cudaFree(group1_Ev_Ib_d)); 
	CUDA_SAFE_CALL(cudaFree(group2a_Eb_Ib_d)); 
	CUDA_SAFE_CALL(cudaFree(group2a_Ev_Ib_d)); 
	CUDA_SAFE_CALL(cudaFree(group2_Eb_1_d)); 
	CUDA_SAFE_CALL(cudaFree(group2_Ev_1_d)); 
	CUDA_SAFE_CALL(cudaFree(group2_Eb_CL_d)); 
	CUDA_SAFE_CALL(cudaFree(group2_Ev_CL_d)); 
	CUDA_SAFE_CALL(cudaFree(group3b_d)); 
	CUDA_SAFE_CALL(cudaFree(group3v_d)); 
	CUDA_SAFE_CALL(cudaFree(ngroup4_d)); 
	CUDA_SAFE_CALL(cudaFree(ngroup5_d)); 
	CUDA_SAFE_CALL(cudaFree(ngroup6_d)); 
	CUDA_SAFE_CALL(cudaFree(ngroup7_d)); 
	CUDA_SAFE_CALL(cudaFree(ngroup9_d)); 
	CUDA_SAFE_CALL(cudaFree(group8_Ev_Ib_d)); 
	CUDA_SAFE_CALL(cudaFree(float_d8_d)); 
	CUDA_SAFE_CALL(cudaFree(float_d9_d)); 
	CUDA_SAFE_CALL(cudaFree(float_d9_2_d)); 
	CUDA_SAFE_CALL(cudaFree(float_d9_4_d)); 
	CUDA_SAFE_CALL(cudaFree(float_d9_5_d)); 
	CUDA_SAFE_CALL(cudaFree(float_d9_6_d)); 
	CUDA_SAFE_CALL(cudaFree(float_d9_7_d)); 
	CUDA_SAFE_CALL(cudaFree(float_da_d)); 
	CUDA_SAFE_CALL(cudaFree(float_da_5_d)); 
	CUDA_SAFE_CALL(cudaFree(float_db_d)); 
	CUDA_SAFE_CALL(cudaFree(float_db_4_d)); 
	CUDA_SAFE_CALL(cudaFree(float_dc_d)); 
	CUDA_SAFE_CALL(cudaFree(float_dd_d)); 
	CUDA_SAFE_CALL(cudaFree(float_de_d)); 
	CUDA_SAFE_CALL(cudaFree(float_de_3_d)); 
	CUDA_SAFE_CALL(cudaFree(float_df_d)); 
	CUDA_SAFE_CALL(cudaFree(float_df_4_d)); 

	unsigned long long end_cleanup = read_timer();

	printf("elapsed time for init: %ld\n", end_init - start_init);
	printf("elapsed time w/ memcpy: %ld\n", end_mem - start_mem);
	printf("elapsed time w/o memcpy: %ld\n", end - start);
	printf("elapsed time for cleanup: %ld\n", end_cleanup - start_cleanup);
}


