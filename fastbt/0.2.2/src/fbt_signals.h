/**
 * This handles all signals and adds some special catch-all signals
 *
 * Copyright (c) 2008 ETH Zurich
 *   Mathias Payer <mathias.payer@inf.ethz.ch>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301, USA.
 */

typedef void (*sighandler_t)(int);
sighandler_t fbt_signal(int signum, sighandler_t handler);
int fbt_sigaction(int signum, const struct sigaction *act, struct sigaction *oldact);
int fbt_pthread_create(pthread_t *__restrict __newthread,
                       __const pthread_attr_t *__restrict __attr,
                       void *(*__start_routine) (void *),
                       void *__restrict __arg);
void initSignals();

