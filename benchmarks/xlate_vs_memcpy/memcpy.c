#include<string.h>
#include<stdio.h>
#include<sys/time.h>
#include<sys/resource.h>
#include<unistd.h>
#include<wait.h>


static inline unsigned long long
read_timer()
{
  unsigned long temp1;
  unsigned long temp2;  
  unsigned long long time;
  asm volatile("rdtsc\t\n"
	       "movl %%eax, %0\t\n"
	       "movl %%edx, %1\t\n"
	       : "=m" (temp1), "=m" (temp2)
	       : 
	       : "%eax", "%edx");

  time = (((unsigned long long)temp2) << 32) + temp1; 	       
  return time;
}


int main()
{

  char str1[15];
  char str2[15];
  unsigned long long start_time;
  unsigned long long end_time;
  
  start_time = read_timer();
  memcpy (str2,str1,15);
  end_time = read_timer();
  printf("Time taken : %lld\n",end_time - start_time);
  return 0;

}


