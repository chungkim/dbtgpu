#include <stdio.h>
#include <pthread.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <ctype.h>

#define THREADS 5 
int count[THREADS];
int pcount;

void* func(void *x)
{
	int k = *((int *) x);
	int i;
	//for (i = 0 ; i < 10000 ; i++)
	while (1)
	   printf("Thread %d - %d\n",k, count[k]++);
	return NULL;
}

int main()
{
	pthread_t thread[THREADS];
	int thread_no[THREADS], i;
	pid_t pid = fork();
	if(pid){
		printf("In parent; cid = %d\n",pid);
		//for (i = 0 ; i < 10000 ; i++)
		while(1)
	   		printf("Parent thread - %d\n", pcount++);
	} else {
		printf("In child\n");
		for(i = 0 ; i < THREADS ; i++) {
			thread_no[i] = i;
			pthread_create(&thread[i], NULL, &func, &thread_no[i]);
		}
		for(i = 0 ; i < THREADS ; i++) {
			thread_no[i] = i;
			pthread_join(thread[i], NULL);
		}
#if 0
		for(i = 0 ; i < THREADS ; i++)
			printf("*****Thread count of %d is %d\n", i , count[i]);
#endif 
	}
	//printf("*****Parent count  - %d\n", pcount);
}
