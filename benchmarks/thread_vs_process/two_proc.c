#include <stdio.h>
#include <pthread.h>
#if 0
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <ctype.h>
#endif 

#define THREADS 1
unsigned int count;
unsigned int pcount;

#if 0
void* func(void *x)
{
	int i;
	//for (i = 0 ; i < 10000 ; i++)
	while (1)
	   printf("Thread - %d\n", count++);
	return NULL;
}
#endif 

int main()
{
	pid_t pid = fork();
	if(pid) {
		while(1) 
			printf("Parent - %d\n",pcount++);
	}
	else {
		while(1) 
			printf("Child - %d\n",pcount++);
	}
	return 0;
}
