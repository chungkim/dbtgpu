/*
 * Copyright (c) 2005, Johns Hopkins University and The EROS Group, LLC.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above
 *    copyright notice, this list of conditions and the following
 *    disclaimer in the documentation and/or other materials provided
 *    with the distribution.
 *
 *  * Neither the name of the Johns Hopkins University, nor the name
 *    of The EROS Group, LLC, nor the names of their contributors may
 *    be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* This file has the functions that need to be inlined both in xlcore.c
   and in emit.c */

#ifndef INLINE 
#define INLINE
#endif

inline static bb_entry *
make_bb_entry(machine_t *M, unsigned long src, unsigned long dest, unsigned long p) 						
{						
  bb_entry *new_entry;

#ifndef PIC_BB_DIR 
  bb_entry **lookup_table_entry;	
#else
  unsigned long* lookup_table_entry;
  unsigned long new_entry_entry = M->no_of_bbs;
#endif

  if(M->no_of_bbs >= MAX_BBS)	
  panic("MAX_BBS exceededn");

  new_entry = &((M->bb_entry_nodes)[(M->no_of_bbs)++]);
  new_entry->src_bb_eip = src;

  new_entry->trans_bb_eip = ABS_TO_REL(dest);

  new_entry->sieve_header = NULL;
  new_entry->proc_entry = p;

  lookup_table_entry = &M->lookup_table[(src) & (LOOKUP_TABLE_SIZE - 1)];	

  new_entry->next = *lookup_table_entry;

#ifndef PIC_BB_DIR 
  *lookup_table_entry = new_entry;
#else
  *lookup_table_entry = new_entry_entry;
#endif

#ifdef PROFILE_BB_STATS  
  new_entry->trace_next = NULL;
  new_entry->nInstr = 0;
  new_entry->flags = 0;
#endif

  return new_entry;
}

inline static void
set_start_eips(machine_t *M, bb_entry *curr, unsigned long src, unsigned long trans)
{						
  curr->src_bb_eip = src;
  curr->trans_bb_eip = ABS_TO_REL(trans);
}

inline static void
set_end_eips(machine_t *M, bb_entry *curr, unsigned long src, unsigned long trans)
{						
  curr->src_bb_end_eip = src;
  curr->trans_bb_end_eip = ABS_TO_REL(trans);
}

#ifdef USE_SIEVE
INLINE void
bb_setup_call_calls_fast_dispatch_bb(machine_t *M)
{
  /* Emit the special BB that transfers control from 
     one basic-block to another within the basic block cache. */

  /* PUSHF Already performed by Call */

#ifdef PROFILE
  bb_emit_inc(M, MFLD(M, ret_miss_cnt));
#endif

  /* 1. Save EAX */
  bb_emit_byte (M, 0x50u); 	/* PUSH r32: EAX */

  /* 2. Save EBX */
  bb_emit_byte (M, 0x53u); 	/* PUSH r32: EBX */

  /* 3. MOV EAX, M->EIP */
  bb_emit_restore_reg_from (M, REG_EAX, MREG(M, eip));

  /* 4. MOV EBX, EAX */
  bb_emit_byte(M, 0x8Bu);       
  bb_emit_byte(M, 0xD8u);       /* 11 011 000 */

  /* 5. AND EBX with NBUCKETS-1 (to get the remainder of dividing EBX by NBUCKETS, i.e., to get the hash value) */
  bb_emit_byte (M, 0x81u);	/* and ebx, imm32 */
  bb_emit_byte (M, 0xE3u);	/* 11 100 011 */
  bb_emit_w32 (M, NBUCKETS - 1);

  /* 6. Finally, perform the Jump to &(M->hash_table[%ebx * 8]) to pass control to the code at the bucket */
  //bb_emit_byte(M, 0x65u); /* GS Segment Override Prefix - for accessing the M structure */
  bb_emit_byte (M, 0x8Du);	/* lea %ebx, M->hash_table[%ebx * 8] */
  bb_emit_byte (M, 0x1Cu);	/* 00 011 100 */
  bb_emit_byte (M ,0xDDu);	/* 11 011 101 SIB */
  bb_emit_w32 (M, (unsigned long) M->hash_table);

  bb_emit_byte (M, 0xFFu);	/* jmp %ebx */
  bb_emit_byte (M, 0xE3u);	/* 11 100 011 */
}

INLINE void 
bb_setup_fast_dispatch_bb(machine_t *M)
{
  /* Emit the special BB that transfers control from one basic-block to another within the basic block cache. */
  /* 0. PUSHF */
  bb_emit_byte (M, 0x9Cu);

  bb_setup_call_calls_fast_dispatch_bb(M);
}
#endif
