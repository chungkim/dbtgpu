#!/usr/bin/perl
##########################################################################
#
# Copyright (c) 2005, Johns Hopkins University and The EROS Group, LLC.
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
# 
#   * Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in
#     the documentation and/or other materials provided with the
#     distribution.
# 
#   * Neither the name of the Johns Hopkins University, nor the name of
#     The EROS Group, LLC, nor the names of their contributors may be
#     used to endorse or promote products derived from this software
#     without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
##########################################################################

use warnings;
use strict;

open F, 'DBG';
my($D, $myaddr, $dest, $entry, @entry_arr, @dest_arr, @call_arr);
while(<F>) {
    ($D, $myaddr, $dest, $entry) = split;
    if(($D eq 'D:') || ($D eq 'I:')) {
	for(my $i=0; $i < $#entry_arr; $i++) {
	  my $a = $entry_arr[$i];
	  my $b = $dest_arr[$i];
	  my $c = $call_arr[$i];
	  if((hex($a) == hex($entry)) && 
	     (hex($b) != hex($dest)) ) {
		print "DUP entry = $entry, (c, d) =  ($myaddr, $dest)";
		print " vs  ($c, $b) \n"
	  }
	}
	push(@entry_arr, $entry);
	push(@dest_arr, $dest);
	push(@call_arr, $myaddr);
    }
    elsif($D eq 'R:') {
	pop(@entry_arr);
	pop(@dest_arr);
	pop(@call_arr);
    }
}
print $#entry_arr;
