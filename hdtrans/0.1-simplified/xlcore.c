
bb_entry * 
xlate_bb(machine_t *M)
{
	/* ... initialize ... */

	/* This loop executes once per instruction */  
	while (ROOM_FOR_BB(M) && MORE_FREE_PATCH_BLOCKS(M)) {

		/* Decode the Instruction */
		if (do_decode(M, &ds) == false) {
			panic ("do_decode failed");
			break;
		}

		unsigned char *begin_bbout = M->bbOut;

		/* Emit the Instruction using the appropriate emitter */
		isEndOfBB = translate_instr(M, &ds);

		if (isEndOfBB)
			break;

	} /* Grand Translation Loop */

	if (!isEndOfBB) {
		if(M->curr_bb_entry->trans_bb_eip == (unsigned long)M->bbOut)
			M->curr_bb_entry->trans_bb_eip = NOT_YET_TRANSLATED;
		bb_emit_jump(M, 0);
		M->patch_array[M->patch_count].at = M->bbOut - 4;
		M->patch_array[M->patch_count].to = (unsigned char *)M->next_eip;
		M->patch_array[M->patch_count].proc_addr = M->curr_bb_entry->proc_entry;
		M->patch_count ++;
	}

	/* ... */

	M->jmp_target = (unsigned char *)bbStart; 

	return(curr_bb_entry);;   
}

INLINE bool 
translate_instr(machine_t *M, decode_t *ds)
{
	return (ds->emitfn)(M, ds);
}
