
typedef struct { 
	
	unsigned long index;	/* sanity check */

	char *disasm;			/* dissassembly string */

	OpArgument args[OP_MAXARG];

	CONST_TABLE void *ptr;

	const char *emitter_name;

	unsigned long attr;

	unsigned long flag_effect;	/* Whether the instruction sources/sinks/both the flags */
	
} OpCode __attribute__ ((aligned (32)));

const
OpCode nopbyte0[256] = {
  { 0x00u,  "addB",   Eb,   Gb,   NONE, EMIT(normal), 	AX, W },
  { 0x01u,  "addL",   Ev,   Gv,   NONE, EMIT(normal), 	AO, W },
  { 0x02u,  "addB",   Gb,   Eb,   NONE, EMIT(normal), 	AX, W },
  { 0x03u,  "addL",   Gv,   Ev,   NONE, EMIT(normal), 	AO, W },
  { 0x04u,  "addB",   AL,   Ib,   NONE, EMIT(normal), 	XX, W },
  { 0x05u,  "addL",   eAX,  Iv,   NONE, EMIT(normal), 	XO, W },
  { 0x06u,  "push",   ES,   NONE, NONE, EMIT(normal), 	XO, N },
  { 0x07u,  "pop",    ES,   NONE, NONE, EMIT(normal), 	XO, N },
  /* 0x08 */
  { 0x08u,  "orB",    Eb,   Gb,   NONE, EMIT(normal), 	AX, W },
  { 0x09u,  "orL",    Ev,   Gv,   NONE, EMIT(normal), 	AO, W },
  { 0x0au,  "orB",    Gb,   Eb,   NONE, EMIT(normal), 	AX, W },
  { 0x0bu,  "orL",    Gv,   Ev,   NONE, EMIT(normal), 	AO, W },
  { 0x0cu,  "orB",    AL,   Ib,   NONE, EMIT(normal), 	XX, W },
  { 0x0du,  "orL",    eAX,  Iv,   NONE, EMIT(normal), 	XO, W },
  { 0x0eu,  "push",   CS,   NONE, NONE, EMIT(normal), 	XO, N },
  /* 0x0f is a two-byte opcode escape */
  { 0x0fu, TABLE(twoByteOpcodes) },

  /* ... */
};

const
OpCode twoByteOpcodes[256] = {
  /* 0x00 */
  { 0x00u, GROUP(ngroup6) },
  { 0x01u, GROUP(ngroup7) },
  { 0x02u, "lar",     Gv,   Ev,   NONE, EMIT(normal),	AO, W },
  { 0x03u, "lsl",     Gv,   Ev,   NONE, EMIT(normal),	AO, W },
  { 0x04u, RESERVED },
  { 0x05u, RESERVED },
  { 0x06u, "clts",     NONE, NONE, NONE, EMIT(normal),	XX, N },
  { 0x07u, RESERVED },
  /* 0x08 */
  { 0x08u, "invd",     NONE, NONE, NONE, EMIT(normal),	XX, N },
  { 0x09u, "wbinvd",   NONE, NONE, NONE, EMIT(normal),	XX, N },
  { 0x0au, RESERVED },
  { 0x0bu, RESERVED },
  { 0x0cu, RESERVED },
  { 0x0du, RESERVED },
  { 0x0eu, RESERVED },
  { 0x0fu, RESERVED },
  /* 0x10 */

  /* ... */
};

/* ... more OpCode tables: about 7k total */
