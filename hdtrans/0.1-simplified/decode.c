
bool
do_decode(machine_t *M, decode_t *ds)
{
	/* ... initialize ... */

	/* First, pick off the opcode prefixes. There can be more than one. */
	for(;;){
		ds->b = istream_peekByte(M);
		if (IS_IFAULT(ds->b))
			goto handle_ifault;

		ds->attr = table[ds->b].attr;

		if ((ds->attr & DF_PREFIX) == 0)
			break;

		ds->no_of_prefixes ++;

		switch(ds->b) {
			case PREFIX_LOCK:
			case PREFIX_REPZ:
			case PREFIX_REPNZ:
			ds->flags |= DSFL_GROUP1_PREFIX;
			ds->Group1_Prefix = ds->b;
			break;

			case PREFIX_CS:
			case PREFIX_DS:
			case PREFIX_ES:
			case PREFIX_FS:
			case PREFIX_GS:
			case PREFIX_SS:
			ds->flags |= DSFL_GROUP2_PREFIX;
			ds->Group2_Prefix = ds->b;
			break;

			case PREFIX_OPSZ:
			ds->flags |= DSFL_GROUP3_PREFIX;
			ds->opstate ^= OPSTATE_DATA32;
			ds->Group3_Prefix = ds->b;
			break;

			case PREFIX_ADDRSZ:
			ds->flags |= DSFL_GROUP4_PREFIX;
			ds->opstate ^= OPSTATE_ADDR32;
			ds->Group4_Prefix = ds->b;
			break;
		}

		/* Note that the prefix bytes are not copied onto the instruction stream; Instead, they are just 
		copied onto their respective placeholders within the decode structure */
		istream_nextByte(M);
	}

	/* Pick off the instruction bytes */
	ds->instr = (unsigned char *)M->next_eip;
	for(;;) {
		ds->b = istream_peekByte(M);
		if (IS_IFAULT(ds->b))
			goto handle_ifault;
		pEntry = &table[ds->b];
		ds->attr = pEntry->attr;
		istream_nextByte(M);

		if (ds->attr & DF_TABLE) {
			table = pEntry->ptr;
			//ds->attr &= ~DF_TABLE;
			continue;
		}

		if (ds->attr & DF_FLOAT) {
			wierd = 1;
			table = pEntry->ptr;
			ds->b = istream_peekByte(M);
			if (IS_IFAULT(ds->b))
				goto handle_ifault;
			istream_nextByte(M);
			ds->modrm.byte = ds->b;
			if (ds->modrm.parts.mod == 0x3u)
			{
				pEntry = &table[ds->modrm.parts.reg + 8];
				ds->attr = pEntry->attr;
				if (ds->attr & DF_ONE_MORE_LEVEL)
				{
					table = pEntry->ptr;
					pEntry = &table[ds->modrm.parts.rm];
					ds->attr = pEntry->attr;
				}
			}
			else
			{
				pEntry = &table[ds->modrm.parts.reg];
				ds->attr = pEntry->attr;
			}
		}

		break;
	}

	/* ... lots of conditional branches ... */

	return true;
}
