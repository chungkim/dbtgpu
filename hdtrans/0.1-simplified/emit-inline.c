
bool
emit_normal(machine_t *M, decode_t *d)
{
	unsigned i;
	unsigned count = M->next_eip - d->decode_eip;

	memcpy(M->bbOut, (unsigned char *)d->decode_eip, count);
	M->bbOut += count;

	return false; 
}

INLINE void
bb_emit_byte(machine_t *M, unsigned char c)
{
	if (M->bbOut != M->bbLimit)
		*M->bbOut++ = c;
}

INLINE void
bb_emit_w16(machine_t *M, unsigned long ul)
{
	bb_emit_byte(M, ul & 0xffu);
	bb_emit_byte(M, (ul >> 8) & 0xffu);
}

INLINE void
bb_emit_w32(machine_t *M, unsigned long ul)
{
	bb_emit_byte(M, ul & 0xffu);
	bb_emit_byte(M, (ul >> 8) & 0xffu);
	bb_emit_byte(M, (ul >> 16) & 0xffu);
	bb_emit_byte(M, (ul >> 24) & 0xffu);
}

INLINE void
bb_emit_jump(machine_t *M, unsigned char *dest)
{
	unsigned long next_instr;
	unsigned long moffset;

	bb_emit_byte(M, 0xe9u);

	next_instr = (unsigned long) M->bbOut + 4;
	moffset = (unsigned long)dest - next_instr;
	bb_emit_w32(M, moffset);
}

INLINE void
bb_emit_call(machine_t *M, unsigned char *dest)
{
	unsigned long next_instr;
	unsigned long moffset;

	bb_emit_byte(M, 0xe8u);

	next_instr = (unsigned long) M->bbOut + 4;
	moffset = (unsigned long)dest - next_instr;
	bb_emit_w32(M, moffset);
}

/* ... more emit functions ... */
